export const testData = `abc

a
b
c

ab
ac

a
a
a
a

b`/* .replaceAll('\r\n', '\n') */.split('\n\n')
	.map(x => x.split('\n'))
	// to support dos and unix line endings
export const inputArr = `ymw
w
wm
vsw
wm

vs
lqn
ti
uvl

fryuv
pngtvuhfr
fcbrulv

pr
rp

tdvcspxnujak
vektaowjncs
ifybztmhlasqrjkgc

exvbqomlucjfi
cufeiomgvl
uolcfvmie
eoculrifnmv
fvclmdnoeiu

iyomzvhpdjtrw
rpnovdhwzmityj

emplyqxzfjrwb
xrezwjlsqpb
prdvzbhljqxaw

wtuolk
kwtluj

kixnqubme
adjgfwvl

lkzqupo
nsfhbc
rki

gkmqehib
ngiemuh

m
s
s
w

tu
arudi
gukp
u

hkfs
hpfjk

yzkuhrncx
dqasvifwne

x
x
x
x
x

zabmrcequtghsnlvdpkfij
drbelmtjasiupnhqgfvkc
hdvlprtebgamqfknijycsu
pcirvatkgmhndefulbqjs
hinfqkldbzcretsuvamjpg

eoc
omvqig
aglo

qgnjkcirewz
vmwhuafoxpt

zinoybesugqjhr
fzgeasrhynq
myzngtrwqvl

gdma
dq

haugzib
xeghuf

fsl
fsg
fs
fts
sf

bysucwlra
ucwylrbas
sbkuarlcwy

skmphtzonuydfxi
xpidmolthzywfusg
hxtysznfimdpou

gonabqykhrzmpsl
pyrblhznoswkgqam
pgbohakrqzslmny
ogbqmyprlnkahzs

hdcl
cdlh
hdcl
hldc
ldhc

fiwztpbcxreqodunljvyh
fsrmnugobhydpkc

fcloanbrm
dnkaglrxot
swvylepanjhqzoui

upali
kyilvspz
rlejqntbmoxi
iklfdw

tdxwaqsybovjgm
bmxjznqdt
ujtmehxqpldb
qbltpkrxjmd
qjxdzmbit

avnituwozrgcjmsd
rwsaocnugzjmdtv
zanrotwdusjgcvm

duqy
iu

rnatpelzuiv
eaizntulvr
fntbrilvaezu

vwprtg
nfrwluaodtphg
gtrszpw
writqgmp

ayxievonwmtchp
aytxowvcpmihe
xtehvomypcwifa
hwmoytcvipjxae
yvawtmeiocxfhp

dnrpaivezcktfwy
vdpnyfzrmcktwi
icfnyrtpzdkwv

gejt
getj
ejgto
jtge

btmwidzrun
mtunzdrbi
oiebudtxkrmyz
ztmbdrui
dmzbrtui

rasxhidbgw
klxwvhbeacidsg
dhxlacgusirbn
sqabjmhtifxodg

utpkgqjiycexosvanwl
grjfvnbuscwadmihxzk

ktnhpvqxzfuwosmgl
fwxmtvzohnrlkgsup
hvlzfroxucmgpksnt
pslyfovbigtukzxnmh

zde
zed
zed

v
v

calfotjxbg
etagbc
brackuvtzpgi
mdhapbtygcv

hkdb
eahrxg
pohs
hujbq
dchbt

nw
ewgr
amyh
pquwn

o
o
o
o
ou

oakzcxnp
xoclan
oxazcknp
hwcomfxna

iymzjovgkdrqpalehubc
ptcgyqhrboazuv
vxyohunafqcswzgprb

azwb
zabw
bzaw
zabw

zu
zxi
fldo
u
p

ejhtlbm
etblhm
mtbelh
hbltme
thbelm

tnqxahufm
bhftnxumaq
ufbahtqnxm
amnhfxtuq
qfxumwanht

vgdtjm
tdga

wkzbx
tksxbwz

niasv
cirenka

pvbjswzixkcug
bxvjwsiugck
xcwjbkisugv
bgikucvdsjxlw
aswuxibjgcpvk

vrjcpfuhetkd
mjbhzwctpeyk
qhwepmtljksc

mkdb
bkmd
omkdb
kmdub
dmkb

tmharzskfycvnio
abdmulpwjyxeqgc

skmacnpgzho
ghsnpaczkmo
haposzmgnkc

mrngvewdsqca
tfjyqhkxes

ampi
m
mt

kcduvyaqgtpfmij
vxgfiqcdlajypuk
gcyoeufqkpdijmav
cvfuqpiyeadjkg

tblyemri
teburlymi
tneidylbrmfc

jtmkxgehipfuasqrbwvcdyn
azwkvfhsbynrjpgmutdcqex
uobjpcgzstharyvkxefmnwdq

etgikbzjd
zdybhkwteqig
ouitdbrfgeznvkxs
bekipadqgtzlc

v
ale
c
h

neqvowybzx
nybezqwxvo
zbenxwvoyq
vqbxwznyeo

kwdm
k

eahrozxdj
aopndezjx
emjaonxyzd
jxeaufdosz
yaoedxjz

hzwc
s

bnemwzv
mbzwnve
envbamzw
ebnwmvz
ezvntwbm

djkqnrcvpseohazmflb
jyzftduhbnparmlscqovk
zksbneljmxcfdprwaovgqh

zxfgtveb
xfbetvgz
txevgfbz
tfxbvegz

mfcr
cfmr
mfcr
frmc
rfcm

qsdvzyixl
zvdlqy
znydvqlku
lvndzyq

vpok
hoyb

lucohageisvdtfqnbrxk
naekdgslcqoftxrhbvui
oxgrunehtafkbidqclvs
vrdsfanugtihkxqlcobe

poayngfzwb
mzyi
clxuesdvqythrjk

vtsxloimpq
sqvxulbiomtp

xlo
xol
lox
lxo
lox

mqtafugsj
wahiptqjbe

nuosc
usocn
cnuso
socaun

rwchjlkmidbtxupfvg
jkudgvprhalcfbnymtw

uptazbcdxywqf
buqywadtfhcx
klbduawtqyx

klfyrxdzn
lsnrcyvzokhx
eafrzqkxly
xzlqryk
xwriykmljzugp

gfvxdi
vtwigxdj
xdnvgyi

vs
sepvngak
vs

dqzfx
xqdfz
qfdzx
qxzfd

lfdksujt
ijcdtslkf

rxqevdwishmjfluy
lwshvauecgrfiqxyjm
txisrubqelyfwvmhj

ijlvuenzdsykfwc
qkxacuydnehzbglsrfm

purkflqgovscyedzhtinxa
ensuvzrftpdgcxykhqilo

f
p
jkxa
p

rwqcyxtbvn
rtbwcxqnyv
ytwvnbxqrc
vwxcrqtnyb

takohlgzesf
katgsozeflh
aolgtesfhkz

jakvrlxipqgwn
xwjhigvkqarlpn
vrlitwjqcagnkxp
ngcljqvpkrwiax

csg
sgc
sgc
gsc
sgc

xftrpzkynwjcils
tyzjfncspxwilrk
clxjtnsriykzpwf
rcflzkwpxjynsti
ntwxsapejzkcrilfy

gmabr
mrbag

idrvzjbafosemw
fvewdrjazsmibo
woisjrtdvxezkabmuf
aifmbdjowzvres
jzesabwvmdiofr

cvsr
scvr
srvc
zcrsv
rscv

ldjs
sdjl
sdjgl
ldjs
jlds

vf
vf
vf
vf

nqi
gmiywb
il

bsvamr
qkvrab
abrqv
ktabvhr
rvbhax

cpgsqnorkay
yproncgkqsa
okapcgqsnyr

fhcrk
wjaxbchs
flunch
khorczi

uyj
jyau
yuj
yju
ykjiu

yuqwkzf
jaocfzyxpw

ayqun
vwpzfoj

ftkx
nftjkr

saupfiylrhckzdmq
idwlp
ldip
wlidjp

qhiu
suqni
iuxnq
qui
iumqzg

xvoqnda
dloqvnxa
donvqax
vqdnoax

ctblovfpygsmwqrjai
ivfwqgrpsjtacm
pmvqtjfragcwis

m
ms
m
m

v
vxcjl
tvgn

agcvutldfnwpoqhe
panwfvugetxqo
tusfwgmoaeyvnjkpq
wvutgfoeraqmzpn

pfelnjvha
qmzexonlv
rbyiugdswk

spmzxl
nhbuvqcagdi

vpfmjgitk
vefgrdkpit
gfitvkbp
pftjvgik

eiczmbkfargtosjdlyhwuxqn
uiqdwkptonjzxlfsrgemahycb
mnfzskoytqlbhuidjagwexrc

ylcroxz
azolxycr
rcxoylz

ipjvbtacrsuklozxqg
gkczovaibqptulxsjr
zsuctpolbxjikqrgva
aglbcjqsvzxokrupti
tlpurqvxgazoibjsck

chjtiyplzb
rzcplthisq
lcwgmnzvhei

uvbqnetmkdsg
tcgizb

anet
tnea

b
lbm
b
b

rjsgkvxnoctybf
onhbkjryzvtcfag
rvtbyfgxsonkjc
fcgoktjnbryv
cvgpjdnyrotfixbk

woyzins
vml
l
ekh

imurfjexb
iufxme
midneuxf
ztudmxife
kymfxieuod

fjevlykwshit
jfpwistkglev
ljidewskvtf
orqwblzsevtcikjfu
xayemltwksfjivn

jmfbqzsu
jsxbiumfv

kuytgfqzwolpxdcnjhrsme
xwzjpqsgfmdklhytocuren

tswk
ztqw
mdrpthif
t
tw

rnskdcauhe
rukeyndhasc
ahsrknuecd

mxsaerbdhgqcy
xvqybdmahreos
qyxbmgjsradhe
bqxheasyirmd
rdqehysabmx

bkzxw
xzkwb
xkwbz
wxjzkb
bwzxk

avlwfyqrp
jkozmedhgubsxnct

ie
gkec
epx

xvbisfp
zxprafsim
pwiqhkfucsx
sflxmjbivyp

mkhg
dpnflvzhesrt
bmhu

mpl
pml
pml
pml
pml

hdrtnziabqscwxp
btdczqxhuwranspi
prwatsxhzbdnicq
hxrnzawpcsqbitd

gxzafpjvciywskn
wnzvjifscrxykapog
ncsjiapyzkgvxwf
jkexzingwpvscayf
iyjzfwnckavpxsg

xtdblwyqfsjak
uwtsncvjlm
switjlm

j
je

ce
c
cy

semxdiqw
lbnmaq

n
n
n
n

rq
rqes
qr
rq
qr

ibndxgtuawhokzc
wkxhgdcznjtoaiubp
uxzwtdnblapkchogi
zhtidxwvecobkgyuna
cgabuhrpioxkwdztn

ekcrfujsopgvbwxtya
jeykusfvpxbowacrztg
frkjsygapcowxubetv
shjoyanumqkertxgbdfwpvc

mfw
pfwm
mwfi

ocyhq
chqye

nzsxagpfuhmqkcy
flyqunphxm
qmpnhxufy
mjurwfpqhnyxe

pxqf
pqx
xpq
qgpx

bzhprgot
rzhpaoug
rmhygacqxvkpjzo

tlzpvdgkafqw
clqvpgtkafd
lgetfbksxivdprqah
kuptalqgjfdnv

rfkwoalbjdi
bkilwjfro
orwjlkibf
okbdflrjiaw
nrwjzoibflk

n
n
n
n

vznusdgkmcrxfpwojtleqhy
rveftohngkxzdljq
lefikohvbxqrjtdzng
xqtnjzvdglfkrheao

xohambwtulqnpgf
ruxmopbjwtfncdgal
bnsptuwyvlmxzogif

azbsgpwihlcfmuk
ahfgulbckzsm
gslafkcombuzhj
hujmklzgbafcs

nzdqthy
wjeagzcxyiq
qzfycp

hudclx
quczdxhl
chudlx
hlcxdu

nrmypdbzvolxga
ykgtvfcasz
yagwvqze
zfvygja
iyzgauvht

eitov
aizw
qjai

ruptmgzobxa
hjvsdwylieqnkf

njsgbwltpyeohf
gxsojntpyfwlehb
fontbepsjwgyh
mdfhypjeobsriktqanwug

pcunvqdgxjfzokwm
awighqdejvnflp
iqdnvfpjwlgs

upqz
zhyxjdtlv

nq
qpns
nqfu
qnps

lve
pi
ipf

uafxbjp
lfpjcw
rjfp

obh
h
h
h
h

ezx
xze
xze
efxz
zxe

yshdxqelnzmuipowr
oqsedljrinmytwuphx
ljnbcirxshyouqdzewp
wbiqxtrducehoylspn
rvshygoewlqunidpxfak

e
e
e
e

hyuwjlgxoa
chajowynxlf
wyfhlnjaox
aztohyxlwj

eqldmuxigyhktfwbczapnrvjso
vpxflgohmqijwadnbykrecustz
kswxetzualphigncdoqjmfrybv
znilqdpkgratwjvuybxhemfocs
izjbnyvastgdchfeoxmrwqpklu

qwjzem
ewzmcqj
cqzwejm
mwqejz
qbmkjdzew

rvuoscwqldtnjfpyk
dstluofwkrqcjnyp
ujnytlcqwsdaokprf

cbhwotxiglrm
iwlhgcbtrom
ibvrotmjscgwh
gkcltbmrhoxwi
tzrgowmcbihx

dc
cd
cd
dc
cd

pnsjqghf
ntqjfhgp
nfphezrjmgv

gotlaypnhj
ylonphtgja
gahjotynlp
lpajtyonhg
lynopgtajh

uqkwycrbd
dubvkqrcywm
uqdkybcrw

hvwialzjfqecpukxdobr
fhkeaczluqwbpvrxdoji
qkabjohpwcurvxlidezf

zjrqhwbkcdv
cvxjhbqkpwdz
omdzlwkbqgcajhv
rjcvqwzhtdkbp

olmvrwtagfuyjbzi
mfrtbuvjizaylgow
vafmjgyubzliorwt
orbmjpvigztwafuyl

rgmodkbs
mogs
suogdrm
togm
mogv

hf
fha
tfh

bcazwpirygekfoxjdtnlmuqhvs
fzkrwyuslvmajxqdienot

xdanjtygfrwhkzblpq
jamxfwzklchngrby
hyvlbzfjrwkonga

shaxbk
rztkmls
nwjvucedk
okt

rpvukdoaqfywlciehjnt
qxwhtregcnilvapskuodf
ltacrhfwquvnizkdjpoey
vtqekcpnwauflhiord
hvqlkurptfaiwnemodc

huqwrpfsilxotcazvygdnj
navdfwistcpzljgruqxoyh
lhdipqtczywaxunbfojvrsg
wotlyvzfjdqgcuarhnxpsi
qfgyonxuzavspjrdhlciwt

qdzjxfhpnmbwtuskyceg
sxyaqfwcjehkprodzvg
iykjhqscdzfpwxoevg
qsveyhjxczwgdolfkp

jf
xn
gpw
k
n

nc
c
c

iyfavqodmwgzspceblxuhk
favzwqtgkyspuixdcbhr

hdcazlv
wtqbg
seu
krbju

myldhzr
wutkednmhla
yidhlvcmr
omhrdgfqblpc
jlsmdxh

omnhyavdercx
qzncxyamro
wmxynozpbauric
kcnwomxyrba
pnocxymrka

bj
ybjk
bj

lgqkmsndwhx
knxsqdmhlgw
msxdqwnghlk
nyxcmgwkseqhldtv

th
qzxyw
a
m
ohu

sujpydikhnv
vnjyhuitksp
ajenhpviskuy

gnjvqlwfxor
fithnsacxjbkmez

rf
uwklarfc
xrf
vrfd
fjr

gtoxeicdfqm
gtiqmeochpdx
tedlycbxqmigao

muxsonhgp
yudpsohm
shpmou
rmpohus
ojmhups

cjsgzxr
axclosqg
cgxs
cgxsj
ixscg

v
v
srgbv
vo

doaphkmvec
adrwme
wytmaesdbun
ieqgzdxfjyam

lcgmfa
agflc
alcfg
faolgk

i
ibe

jemfp
mp
pms
mp

u
u
rda
u

hzdl
xrgdz
vjfgdzr
zknswaeuidcmq
zd

rjwpoqyvzcxektf
xqzwjvetofrcypk
exwjyovkrfzqpct
zctewjxlnorpqakfvy
otxcrkyqfpjewzv

vyhnmjlgekxobps
prgbyltswioquvfmzex

q
feq
dqwx
q
q

i
vbytcon
zksragjq
xdf

lqtzbjudwhm
etgyxpcvarsnko

dupshvrnafz
zafnhrpvodus
vzpsufdnrhxa

ylkzmodvqg
dgmvzqkloy
qkvzgdloym
glqomyzvdk
mldgqkyvoz

v
o
v

actqvguieyrf
uvzgaryipqtl
hrlixgyvatu
dsjokyumvirgwt

q
q

xqln
lqxnu
xqn
oqxvns

i
i
xi
i

cxtnd
dtxcn
vxctdn

vspoaczketurxdwj
vkjlrwznespouxfcadtg
pwsrxvedktacojzu

qdis
sud
dnsquvf
bexgrdsj

mfjcevsywhnuplxiqrzba
ibxrpeunqhzscyfljwmv
yjbqzixfvsupcrhwlenm

wpvmzkhb
zbvkhpwm
mbhyizvwakp

bqfzaeoig
fbzixgaoq
foqizgb
dkibvjquszgofy
ogbfizq

wnz
nwz
nzw

vthxfdk
hfdkgt
vsfdthxkq

uzr
rltz
krfqz

aoshypdlikqfutvebgjnz
qgtdkobisc
rotbdmcwgskqxi

trbziwe
qlmuxkbyngrt

nuqdyjmg
nmgqydu
uqmyicgwnvld

phmc
pd
pd

slj
usqjl

q
q
q
q

zsryajnihfexcguvltk
zvhcanygretxiuk
izcquegtyhxvnakr
nrixgcuaevtyhzk
aeckhzgyqxnvtrui

bkyhnwitqoflxuz
oqhzbntkuwyxfa
hxbywtkqnzouf
qwgkxntozihyubf
nxkzusfeocbtqwy

xwloszbpcatn
pwzblgtanoc
abcngtwlozp
lfzabtnwpoc

paof
paof
aokpf

yermvpgouhilaxtsf
hiuapfsmvergxl
ulemxsipgvhfar
gfimvphualerxs
pusxveifrmglha

qwzfjcbxvhaedmu
hczufqwejvmabdx
bvumhayeoxcqdfzjw
awhvdxnfcqzebjmu
xcfwaqubhdzmjve

p
u

pabrkst
kptxsb
nofjptbglks

pwxtihaoryfqjcmk
yrzwjqiokaevmx
ypmdqrkgxubljownai

wrezth
nbvma

cxz
xzc

botydczsvwgikrfa
otkfryqwmgnacisubzxd
zfcdkyaosrbtvigw

rpestzqxokcb
qrxgimauceftnzkbhdsjw

msuerxvhajnckb
rcahunbemkljsx
cgmsvxrhjbkuna
zxabprinwjmcshku
rucajnktbmxsh

infwrgjphdvokebsqzy
eahcnxpljksfqbwdum
ktdpefljwqnabhs

oilcxdjheuyprna
cpydlhaorjxuie
jzairyhpcouxeldn
hucoibrejxyldap

jbiglmtowxvkdch
ygmjiwtdlohcxkv
cwtfxlhkmsgnvojid
rakpdiolcvxegjtmhwz
chojlykxmvtgiudw

vnqoimutgcwyhx
iwqnecovmyg
dsgmiqeyocwnv

wzjtrynsfqkga
awxkqtnjozdruyg
yngqeajwtzkr

jrg
jgx
jg
gj
gj

ryaxghznduqmkve
kxnrfzbye
nzysjlbriexk
rzfxjclenykpw
teyzrkxwno

mhlaqj
luajh
jhla
jalh

rdbvoqszekg
ekltghbudosivqarz
qkbgsreovzd

r
r
r

il
io
ix
i

nkyrthauqjb
jyktnabhrgq

rkjnluewc
ybgmizxto
efwlprvc

ga
a
a
w
a

goexmwfchknjbliap
hbxcklgnfipajwmot

nwzgbjo
njwyclbsk
nhqjirbwv
fwvbnrjo
wjbnud

thlemzwakn
taylrneqzmh
exsovpcnzitmfua
aetmjzn

hcozjrtsx
sxrczhtoq
ohxgzirsct

yojqc
ocqy
oqycd
aycqo
doqcy

jvuigo
zvujgx

isolkxcghzvfpajdeuyrb
hutcjnmxgyqvrkeow

rocqg
g
gcsx
gile
ngu

jwhoelbgiscfzakmx
wtxkmcoslzigjebafh
fzshoicblxgkemwaj

mubtlkhjspeynw
lkstuweyjmbhnp

tdpy
dypt

set
ts
stol
ts

tfisjzu
tusfzji
fusjizt
szfitju

cwlvjiuqkgom
twedofxgvynjqa

jhuwldvt
twhd
trwdh

mzlqyv
gzydvqm
mejzytqvb
qmybzv

qsdpagrf
ybfuikxm
fjdprszch
jdzcofn

cuemfrdviynwko
ucisbdwftjyxh

wim
wi
wi
wi

velqabhypocmxjs
ckvqlnybrughtmapwdzs

fleghjbtvqrzdpy
njfzrpqyhbkvdlwet
lefdyjvxrhtbqpz

n
qn
dpn

hsdxgiartqfczkmeoy
dmothqjezsfyikgx
wsgndtmezoyikqhfx
dfsxqpotemigyknzh

dhirx
jxbltqdrf
rxnzdyw
xyrdw

gztrajypuesxmonwlhdbf
opjvelthnqxgfkbdsayrzmwu

dsqzkpj
kspqjdyz
qszdpljkm
zjpqksdn

bdvmoeinsw
ldvma
pcytgzqrhufxmjk

qtvdacproxhyfeig
iqevdchgxtypfr
iqyedcpgxfrvht

tnkvcfobej
jicwsrbxeamy

rvqnbulwdgocf
kxgoulzs
mlugyok
kuehijpagtlo

pnayvm
vmpany
mpvnya
ymapvn
manvpy

juandk
bqamunz
cnuxt
oshgeinw
nur

artidpmywzx
bowqmgenkcruj

ymbgfnv
mynvbfgl
bmfyng
mnfgbiy
nfymgb

yc
x
y
ksfv
o

t
gmq
lhrx
zfvb

rzaokythm
madgkjqyhltz
thaozykm

jlecfkhazwyqtrsgn
clrwndzstjfqkgeamh
zenxwahslptgrfcjkq

lhn
qztv
yp

gv
xqg
cwpierytzuh

u
tvfy
uo
j

ywofspubhrgjmcilekv
skejhiorglvbyufw
arezwfukjbgysvhlio

xpnldkub
wkdnqupx
gkyjduxon

qubrihmgokzjclvytdfaw
brueisfpazonxcqk

mykiwzenjtblqasdoux
njuxkbytmwleqridzspa
aubkqjmyinslwdxzte

kjdn
ts
xw
sypu
c

iumldrvwogjbs
ujwsemlrvdibgo

zlknvwbhy
nzms
nitsz
rnzm

ngde
lge
buatigrcvz
xewgy
jlopdg

xjnythqakgsruicbld
ezfwpmov

es
se
nsaer
sxcwplje
gems

r
s
r
r

enkqydmrgjvsbhau
hmbgservnjkuyd
dherisgymjvbunk
beyvnhjxgumsrikd

a
aex
oa
oa

ibedyc
ondfctsjy
cdykvga
ywacdbvuq

mlowux
wqplxc
lhxkwp
pwxl
lxws

truzalwqfpcinjoh
baxwplhjrzvgfynqoic

fdbshvkcm
dcgzskfwb
iadczkfb
ydfrqcobk

gpisnoletr
symv
qhmfws

bjfm
ml
mvcd
mv

levr
v
qv
vq
v

j
j
j

uepmnys
ymgphe

bvlrqpgj
qbvgrpmlj
vpjgrlqcb

fmsuteojyqchwrdv
rdyefmqoschwj

cnudzpksyvbeoxrijqa
iuokqscnjyvpzedarxb
sxijkqybuoavpdrzcen
zckabxsoerjduvpqiyn

qdwinxo
cxabnqgmiwo
ienqflorpxy

tsbxdevwi
evbtwidx
vbextiwd
dvtwixeb

pedufvxjgarqzwy
zvgpfyqrtu
lfzybuscgmvonpr

pgmqjt
ulaib

jefg
vb

ltqevp
grik
qelv

jh
xh
h

diaukxtvohzwnbecsgjr
znyrixahutkgbsopvewjcd
zxsbcivunhqojegrtfakwd
dwuzeagjhtcvrbnikoxs

qnasfbreph
bpka
lpbao
paylb

flmoxzuakb
dhtnicpys

vsnxozukyfqmj
ritpgwelahdbc

ndripfaw
fry
rf
rfy
rf

dqpouiscz
dfouc
wocdlmuneb

coiwvtezuf
tvzofcwui
cfoiwutvz
uvtocwfiz
ntvuoczifpw

aycwonkebidt
vbekioyctad
ckmafdbiyroet

bne
ben
ebny
nheb

bcnvuf
fvubqnc
fuvnbc
bcevnuf

catnl
anclt
catnl
tngcal
nlzcato

pcohyrgfwitkjzxa
pswmtxigkzhlrjafy
rfahwkxzgtjynspi
vhfgpjtinwxakyzr
jebyraqiwfzkgtuhxdp

ifzpyvnrxscqmealb
ypnrofazxsbelqvmci
mzxfpgroayqnbscvlie
cesyrfqvixkbhznpalm
pqsenbmxzrfyvidcal

zyrnivgqhjxsopftadmk
humogbldxfecrpqjwa

lgwkuxemhaifprosq
rgfsxaiwuqhkpmeol
axgfepmsukroliqhw
aiwqlusgfokmehxrp

wmanq
owqpagm
abctmxq

azsnmugchqfv
cqvsmfzebuhagn
qlwafvnxusgmczihd
ekzfcusqhypangvm

jdfau
ufa
auf
ufna
fua

jrvyckoizf
bmka

rvmoylsthqca
ohsriqxyaczt

gkajpilzfeodcunwsvbrhmqtyx
jdqgenacxzrhotlwbvumfkpyis

xewygnkia
xyinagekw
nkayewgix
anyxegkiw

qlykxzcthnprfbej
rlckniyjtzpqbefxh
cxgbtephmkryzjfswqln
hpjtfkcebzyvxlrdnq
ftkyrxblchneqpijz

p
b

vc
t
t
t

vagutfpbqhielzn
bpnligtvqhaefuz
fpevuihnqbatlzg
efvzlhigupqabtn
lufiztepnqghabv

rfbsmiknjogvhyq
mqpikvjrtfonycsbdh
svyfkbhnpmiqrojz
whnsijavbuyqkrfom
kiyjxoqvblmhfrsn

gaxwn
pgmeaw
bfxagw
gwlsa
gkawyc

cxqozuawvliptyjhfs
ahvuqityolspfjwzxc
xifpthoyclaqzvsjwu

syfbj
ysjfeb
fsbj
jbgsfwk

rquip
pjiq
pqi
piq
dqpfikv

p
kiwh
z
fyvmrc

yh
hygs
haye

uqxvaojkbw
jqawxuokvb
vuxbqkjawo

pvzba
bymav
xbyavn
abv

nfasjxqwrgimztvcludob
ldsxumwtircqvzbynofjga
uchrlvstdmixzkjanqgowbf

xbsdla
hupjrknzficy
tveoa

cafoqgjdemsnk
fjivltcdmg
fgdwphcumjb
vyfxjcrgzmid

orciqajldy
ohdqiylacm

y
y
yu
y

li
ilh

lta
avle

wnkha
awnkh
nahw
vhanlwd

cfmurandsiexvwq
vzidurtyboemj

gosh
hoesg
hgotel
gobhj

wransfipukq
riflauznhqbpt
rtvaiunfqocpl

vdajgmsqrpefytzil
txdrisplvafzujk

elwgstkh

epwji
jpfe
hmsryxap

za
abokz
tazefsl
apz
zbar

wxtufcbpdlesrzkvhgmnyjaqo
oeflsvhjwmrznckxdaqgytu

wxydrbvmokunatqpe
xoeawvpqtdbmskrun
ovwxrkuqatdpenbm
wreubavpxmtqdnko
xwpkontbvueaqdrm

npjrtougxm
ptojgmrnx
ngmxpjtfr
jgzxmtrnkp

c
sc
jg

vszrmkhqjpgayc
otswxfuibdl

snwpgtufhaqoxberyjmkdi
tsmbkenrfdujayhwpogqi
fkgsmebvjyanocwurhdqzi

w
s
w
w
ciy

ydxstgncopql
umfrvjawnekibhz

xjw
jwx
xwj
jwx
wjx

kwgxylvafh
fyvhxklawg
klhygafwv
wyfktslgahv
wkxlmyahfgv

git
igt
gti
otgi
git

vnih
vionh
invh

pstycrxzeg
yrcgtzpe
ctpzyrgs
tkyvpcbgzmri
gzpytrc

hvlq
hzj
hz

knrgyxumlio
dyirljutgnv
zagpinwuyhr

mzlvrsgpid
pftrocn
awqprfnhux

wvlpjegodb
dvbegwjpol
lzdwjkfgpobveth

slwmnda
swmdl
sqwdlm

iahz
hzai
iazh
izha

ozaq
zoq
zweqo
zoq

a
qo

vbylpk
gferiaouxwntc
qjk
hqmks

kmtqnhjuorx
gjmutoxqdnr
uomnjxqt
omvpxuytqnj

mdjgyzhnrbqlkixwup
umbnjkwsdolhizgrqpx

estfmkocwlzua
wertfzuslakm
zsxntkfewylm

ewozvybutrlc
pcmtlz
ctflzx
zsamtckl

rcnabkwmljygzdhpos
rpadmzhwgbolknsycj
ojhgrczpwsymklbdan
hmrwybldcskaojgznp

vuyj
rjuy
ugyvjz

gtbvkoi
otkbvg

wzmshqxk
uafswryemj
cvhskzwm
xwohmgqzcs

fezyavinmdjxwqthp
uhmydqjoewfzaxt

ytobegs
rebynogpaw
cgyboueh

hqmen
okqwimt

n
n
n
n
n

mtgye
mezq

njpgvcwbxzqhauds
nghbqsjawpvuxdzc
zsqdahuxwpvbgnjc
vhdaqbspzwgnucxj

sfebtowpijhcl
xtokqyecmwphlid
urtighewvpoza
tbpofwinehd

xmlctywhong
rliwocgutxaeyvn
ptydglnfjzxbso
nogqtlxmywauvh

rjecz
aehocvg
pyufxqbmsltw

wquytmaroexfgzkclvb
umtzxhavjsqgwrckpb

cgvbhlyxtifkzsewjru
rhsftpjgmwabzlcvqkuxone

ndgizukav
zuinkavdg
udzgkvai
wgudikvza

kfvpnot
nvpotkf
ktpvfon
pokfnvt
vfoktpn

kqrd
rdkq
krqd
vfrqkd
qkdr

zyqijxlfdu
zdjfixyq
zihxyfjdq
xoyikqjdbfezg

rq
qur

bozhntm
oeprf

ltx
ij
s
fyvhcdqkm

cqivpm
vmcpqi

ntb
tbfn
nbt
pbtn
cbnpt

a
b
yexo
f
t

tqfazuimjbxgsvedkp
exczvifmsqwakpjtbug
xijzfqcgsvkbpteamy
esorqhmlpnbaijvtgzfxk
evsjiztqmpafgxkb

vhljf
iknboam

kg
jbovgtl
g
gw
iugx

j
j
j
j
j

omsul
ucsim

zykn
aisxbtez
lmzc
zl

a
a
c
a

pkso
noku

rpo
opr

jh
j
j

akqpzhoyrjwitlcxvbdume
mwyrjqzavueokiphxlbtc
yxbrckaepwiomvzqltuhj
voithcspxmwlkyaujbeqrz

naqe
efnq
qne
qenk

chwluymniztgrxafdb
tghnclypeiufsbkwrxam
iynhrfluwgcmadtobx

icgwkoretxmyuzaljdvq
qxgfkuicowleaszyvrdmj
gukrvdczaqojyliewmx
rzosixlycukgdmwejavq
qmuleoaygkrjdicxvzw

erqbuao
peoaiqnf
axoqegwm

ikaydnx
ykja
yksae

jmwfaqxrztivlknscghbu
zkruqhclxainsgtjvfm

mykilgzq
gqzmlki
kwlbiqrzpdgsxj
mgqkuaiznl
cloikfzgyuq

euba
auewb
leabfu

zoqb
boqz
obzq
qbzo
ozqb

lrjvtqf
jrzqltvfha
tlqfjrv

xosvzaq
qsoa
atqso
asolq
aoqs

ckilsdrut
ldrsjtgenqiu

etvizsnywkjobh
jskwoevzltyibnqh

ctkpmiyohjgbuldarn
hoyfbkvdt
zwthxsoykbd

dciagsvnmzyruephk
aslrwitjvenxgcd
vebdqnarctisgl

nlzs
zobwxsln
zsnl
rsiucynlz

gziwqfy
lodmh
knarujextpscbv

uhmobwlgj
obhmwugl
hmabwuglo
oawblgumh
owlbughm

ucsfwbizqdenro
npsrtkmeobfdg

rvhcidqaysukjxbpgzoe
vauhokjrsybiqcdepzgx
cjibvqsxzoyrkeufphagd
cyzpukrhxdsebogajqvi

zcojuprtbhg
pdwbauchjtsgkn
gprtulcfhjyba
xbecghvmjtpiu

qclaiywnujprxdotf
fiuqtwlxocrnayjdp
wdfoacnjipyqtluxr
ejdyblqnihcftpwroavux
nqiayfgucrdjlwxopt

xhzrepycnwsm
iaxfykeblwp

eaqkisdgcojhzw
ckhbasd
ckdhpasb
hcpydabks
sckdha

kn
nk
nuk`/* .replaceAll('\r\n', '\n') */.split('\n\n')
	.map(x => x.split('\n'))

export const agronInp = `v
vx
v
vx
nclmbv

odgpnwqxhbits
pqwsnxihogdbt
pogiwxdhqbsnt

q
o

apulnqresohvktxcymzibdwg
sigdvakmlyxhetopnubwzrqc
iushndtclbeyowxpqmkgzavr

ow
wo

phegq
eqgph
hgpque

xispjzq
sqkzcupi
ikspqyzue

hgfesyvn
dnsvtkhglea
heygnqbcvs

qmrpy
pmrck
vcprum
mrkcp
mrp

kgjx
jghk
kjgu
enlgdwzqkjr
vjgk

ungsdozkphlxy
nbkgzaifsovjpdt
geoskuyznpcd

qw
qw

x
r
sl
v
s

kxcn
kxn
wxtkn
xkcn
xkn

peionmzqyda
apdinomqzey
npmqoiedzay
dmpzoyqiane

gpaedw
mwda
daw
awbd

fyxeptlrmisjhdzquv
xfqydzruvmjepsilth
sipulvdzmftqeyhrxj

kzofrcamenxyvwgpq
zqkrapxwomvycgnef
ywxvpazgcenrqkfmo
kgcvezmfqwrpnxaoy

zhf
zegfhd
fhz
hfz

iqspn
bytmd
sq
gwx

begosk
gnbskz
sgkb

ok
ek
ek
k

hbucsatqvdryfojxlz
tufzxjvbrwasemg

d
ef
k

p
mrp

kuywroetvjnf
wjvtkreufyn
frkyjwevntu
frtewnkjvuy

iu
wzi
axbcgpldyi
seoiz

ekjmbsuwglftpvya
esmwuftgpvyjalkb
vflbtyekmjupasgw
tepmwgksulybavjf
psawvbtgjfelmyuk

jsmoydnvkiql
lqdojsvnik

ayrtxivjlpeuzohnk
whjitnkfyealoguxprzc
bzjiruohtlakxnedypm
pmyxeizqjhkntoarul

vnrcwouidfb
bufrwnociv
unrvefbcwo
cvnswubfro

hmnoazwvtcrigqsujldpk
qeflzdwyongvtjishpacumr

ytwpgkvceaolhrz
agyepkhwlzvctr
yvpkwcgerzhtaq
pcaeygktzwhrv
rgvpkwtylzhcea

io
oi
io
ito

ckmfawyb
glvfiymobadc
jnsfbmapcy
jfysmathnbc

uxbfdygqwpom
sxmuwbdporyvqh
rbuqmwxydpo
ncpuyowqildjxebmk
wxuysoqhvbampd

keo
oz
jdorxbia
lo
ok

zk
zco

bhwmscnrqeou
rlqenzswohbmu

synmfzljkdebq
cmdyzlfekjbns
nkgzsmvlydpjobfute
lsfjkmnzdeby

xshlzwcndtjefi
wcnljfditsexzh
ehtsjwidflcznx

zry
vyqz

owfcybsdipnqlthavu
psdiqfhwctrzvaybon
ybmpdciqhvtsfawon
givjcetkhwbdyponfsaq
cvotsnhqfywplidab

cibwptjnu
ncruhi
hnuic

apzjqbdlwyicfgxv
apxodvwlzyfbgqcij
pcdxjvzwqfliyagb
rixjaclsdpvyzgwqmbfe

timpsox
poxsm

fres
refs
fersq

w
z
r
w
r

xz
x
x
x

ciyhkpjswqvxz
xyskvphmowjzcia
kywsxihpc
wtpnflcsekhbgxud

qrtweigjlbucfxnkos
iwlbgmnosuxfetkrqc
sfbwxcotierqukldn
euroqfdxywclsitkbn
qelfbarchnwuxtsiok

gj
woh
g
jr

lpcdqf
qdfupwzcl
dpcqgtl

zlnkeyroq
rtyoleqnkz
leykztqnor
ahoukcelrxjqynpz

twxjdbuhclzgnop
fgeqkcbvziwtludop

ylukhwtcf
aikdrxfov

ucbklojiyxmz
ulkjco
kroljuch

rfhyj
fhy

cmtedvxioy
infmacweshtuo
tpoimecz

kylduhw
yublkhd
kdulyh
dyuhlrk
zhfgkudly

laoemdcgpyhtnb
ygclebodtmpnha
zomgnqpalhebxwtcd

bmogrnztvyil
vlotrzngbmiy
lvyogrzbimtn

zjcgheblotdramsn
mlgbshrzotcnjawd
otgsjdnmbrchlza
dbazlpfkhgtmsvocnjqr
sgrazcjondmlhbt

ryhifaxpemsgcdubvtjzwnol
bduiojqhcsvlyrxefntpgwma
pvebfwitculrgyjshkdxmaon

lqmopgwbesjhk
mgbkhqejlswop
wqklaeogsdxhfbmjp
sjgphokmqewlb
kesrghbmpjlowq

tu
rp
r
rgh

tfeyhjbnpaqwz
rxuefvgiaybzqth

pwx
lk
iahyv
jr
p

ezsghqtdnjbfox
dsucq
sduqp
cusvdqy
dslwqm

dm
gf
g
xtg

mfihn
mhyifot

iabeswqrzlcxvkpmtnug
qexctgrkwbpusazinmv
riztmgqsewunxbapkcvj
avbuxqizmrkeswtpncg

ozrwipdmgajlkfuxcnhtyveqs
fntcigalzdxmyuesjkqwo
dtfyeqklgjmwxunosizca
efclkowaujqzigbtymdxsn

jkiupmatwxqhdrs
midjhwutq
dmhujzqwyit
dwuqhymtji

ziqfh
hnboqe
qhktzdfp

qohbywgtlucajf
ltahuywfjbmc

arfpqdcbtnw
kfzonycewxbghvr

tdxqelwnu
luenxdtqw
udlteqxnw
wxendqlut

pvkrgaushqidfmnjz
eajzrkhndfitsqvmwugyp
sgohxqcvrzunapjfdmki
vinodpkazgfrmcushjq
jmufirnszpdqoahgkv

uvcmgfrk
fugmrvck
kfuvrmcg
cmrfgkuv
kcvumfgr

kmrb
krbm
rkb
bekrt
rbmk

mzbrfsnxgjpywavcohuqlkitde
xpekistdzlrgmcawnvuqfohjby

vrjew
rvj

qlefnd
fdleo
pxcehwdfy

amftbpvo

ljcyn
cynosjum
jync
yrqbwjzc
jcy

tmdcuk
ckudm
mucdk
cdkum

icsvhmefaztoxkgndq
sbyqivkrdplwjemou

jtrhafpkbwzdox
hbrwpzajokfx
jbkfaphxzcrwon

bqg
qgb
bgq
qbg
qgb

ljte
jle

tvhjx
hvjwxrt
vtjxh
dtjvhx
vjxht

zsdejcrvab
kbjzrscaedv

oehivmku
igfhuxozy

x
xs
x
x
x

klqnrzftv
iqgfaupt
fdqgswyjt

opjeslcx
psohecjxl
yxelsjoc
lescojvx
molnextcsj

sf
cps
fos

crxbjzkivydsgfpehmq
wnhfgljeptzrmikcy

bghydvqxicsfunlzpart
rsyugqcxtvdzanpblihf

v
gnvw
lv
v
lv

mpzvdjqhxnuocr
chidrnuvxmkjqpt
suhdxgvbcnrpmj

ewpfugobznrqydsj
ivgxtjludcm

enytuvzoqmljfwd
satbgwcrndzyk

fuzl
ful
lu
lpuxy

qrhltkesazpmo
puaedmzsqhot

vcktz
zqtjsbuiaomwf
ygzkthx

zt
tdz

fvpjlcarqdukio
luotiqzkjfrwahcdvm
rcykufoadqxilvj

kfpuwxi
wpfxuik

xjzpf
cyqsouwf
xzf
zf
bifpj

q
cw
n
nu
q

lscdtzvhajkqpm
vdpklhqscm
pvcdqskljh
hcjdqklsyvp
eqshcdklvp

muyldxobakfq
ecjvfm

jkas
ws
s
ws

qbjzcolnuy
jyulobzncq
izqlnycoubj

yvsoztedn
eotvydz
yzetodv
edvoyczt

tcfqkxrib
fozbtnlwucq
qvagfdebtc
qcpftbk

amlcyk
acempy
aymcv
cayml

gsybwf
ybsfgw
bsgywf
bfwygs
fsygbw

hypxicwobt
pciytbxwh
xhycspitbw
pcibytrhxw
yckhxtebwpi

olneqtcpzfiahbj
oitbqejcahpnfmzl
bpnjyhfkqmlczteoai
hlpjefoaqznbtci
jdwtzsfcviaqnobhpxel

mkflqourwyh
kqmylohfu
lyohqumfk
fyhukomplq

cfegzaprw
prvbgiymj

besuka
hsnkua
slaguxky

smwuovdpnjfyl
mcjvguors
qjvsmiou
abotmkxujvs
uhmoqsvje

udrmcxqay
dqyhumca
mfwcujskglbedqt

xewpyrsjzktgbaochuni
xjaohuwligysztekpnc
tfghodxyjsnapmquwcziekv

fxypwlo
wpsxleyfo
ylmpxfzwo

ok
rspbkt
mauyxcw
rqdkz

fsehripoyjawt
styheviwfrn

onxrvplyuzqcj
vlzuyrcpxoqjn
nuoqyrzjcpvxl
ljyuoqvnzprcx

o
ac
c
c

hwxmesjuzo
hylvoxzpfbntsr
shoquzx

pidfxg
igrxm

gshkfbcujpyztoevxdlmaw
kzvmejbygdpaxufhlcotws
hfgamkstbwjoyxlczupevd

p
p
ltf

iufdzosnagkhtw
dzkhlgvsitucoa
adtgzokhsviuc

jywe
wy
yw
wzy
yw

ghvadykiscnfxo
noxckgivyhdaf
gfxmtodbwvnkhipaqy

ifsp
xnivluaq
fioe
wisydc
siw

jxbmclnevwztqug
keptqcrmzufglhi
tleqgyvjbumzdc
xzdmslqcutge

ogdxpnjbti
ipgnotx
ipehgonkcxrtfa
ntdiygpox

wxhcjtredknifvulymp
vgksmwqzctybidjofha

lxmficysbt
jslmrn

pcoxigrdtkul
sjmhtbxulyord
uxlcpdktoarv

crgundohktjfasebzvxl
gdsvcufrnljazotkxehb
onbgcjfeirxkhzvudtsla
njcheafbokgzvsxrudlt
ekgvfruchstjldoxzabn

wmkivpgfqdrsl
wmcjvofkhnz
kfwbuvam
jtmwhfkvz

thy
hy
yhe
yh
dqbyh

xtbhkovsjcga
xqkoeavjsch
xacvkjhds
mcjuwskavnrixyh

lhfwqr
ypkruwqs
xcgdwmj

eyiwrundqsflgcavxk
fejmwtyulodkhrzbp

yvz
expa

aohj
ejoai
ldypfnejoa
dcxpjoatv

vuinj
prv
v
vlr

sqpdmrnlwa
nlgasrpqomd
padlqsmrn

r
pjd
d
azbhs

hfotieypnlwdkjzv
jlfveimtdckongwzpy

af
zaf
afn
abf

csjx
scrk
ucws
csj
sjc

juwtder
dtjrwe
werjdt
djreywut
rceoqjwdt

nazufjeomxpkc
xmeopajcunkzf
pfcuazejknmxo
nkzceomfapxju

epiykxforjz
fylzbxjeoipg
ahfyxvpmsujiedzon
eoyxcqwpikjzfg

umfrlqhakzdn
mshjfdkqungrzv

opxjdgemsvailruzfbcq
ozmgbdirxelyfsqpjvuc
iqbucpflxsvzdoetjrm
ysiqolmberzvjnctdxgufp
bxfemdhcukplzrijsqvo

yocg
g
uqg
rzy
vjbmw

vljzumo
ujzvmo
vujomz
vjzomu
zmjuov

srwagukx
ahjroqmxy

cgkj
jcgknvs
jgkyc
kcjgy
jrgck

ugcwvx
gfuzaxw

n
o
n
n
n

vsmd
mcdvbs
cdmxy
tadr

znsokugwejfylica
iwpnfyolqzkcuaegdsj

azuver
remzau
arezu
zerau

wevbmlq
bwalcodevx
ewtuxlbv
zivypeswlb
ebwvaln

qjrdgxmc
hrcozqdxj

mpcogjzyrhisuxlv
mskpazxdfvqc

xtzyl
lxyt
xytl
xlyt
oxslyt

jrteqswkcazlbfhog
tsqafcbgrelzokhjw

fjqlibpwukcoznavyrtd
bzdtupklrqivanwyojfc
lvjarpkyubndzqiwoftc
bdrcniwvktylpajoqzuf
uycprvalqdiwonbjztkf

lwga
dgswz
zgwy
nwugcrik
gwsy

gwmvxakicou
avugcmowixk
ckxugomwvia
ukwacxogmiv
xiwavcgmkou

znvgeymhb
rztconyiq
lwzayn
jswnuzyk
fnwuxlypz

etkdwlgscmzyuafvhbo
vhctfgkmwouaylesbzd
vfcetldzsyobghmuwka
tsgzmkufeajlvwcyodbh
gazdhmkotswlfuvbcey

zdsjweg
sezjgd
dsgzei

zi
z

tbg
fbtg

g
g
g
g

rid
jvi
inxo
id

dbrjtfupcyvzlqmwei
zqbwfmljvcdirtyepu

jmhubvclsdprw
cpswbmldjrveu
rpbsvucmdfjlw

qk
ekniz

kvlmhegzcryd
zrlmdgvefyhc
zhgvrqueymdca
remdfyhzcgv

zxhiaqs
hxiqsa

sumrbd
qgrbuw
ubqgr

dkmqsebjznylwucxot
ewyqhjrlpcfotbis

ti
kxi

dvgmlyepfnhscui
isbvmudynplf
nzqldwmfyuvtpi
finlydpourvme
dulnypvsrafmik

zemctnsralu
trnaelzmsuc
azrstumnecl
mltrceasnuz
rtazlucenms

aydgxnwehomujpv
nedjuaxhopwvmg
dvgwhfojpanmuxe
wjaenpdvmohugx
vanuomdxjehgwp

xchjblnwmogv
coxlngbwjmhv
mjbwoclgxvnh

br
rohb

knpclsvozb
slcvbznk
klcbnvzs
lsvbkncz

cehpgtzmawkjndufrlixb
anrtxfdbhiwmcjzplkeug
rkmwtcunzxhjpgbafdeil

rqlstcwxkmoz
thsvxzkueymrlqw
rqlskomwzpaxt
wcxrzmptqkls

tshv
hfzdlpij
hmces

kfjmaedcoqltuwp
jrngliqdohsce
bgclzdhjqxyero

urndaqxz
fyw
wzqua
ehglomjvbk

guhfvqlnkydpiz
ljgyoiqarzksuwet

yutebcorzh
slxcdkqnhjga

fivxqukh
kixqzvu
vqxuki
xnpiuqvk
kvqfxudie

rpsfdkmc
cdefrksmp
khmsfrdpc
pdmkrcfs

hwn
whn

rxhbzyedclofgwmuktsp
xlmozsbgkcfeyurthdw
psgtkcfazdwomblhyerxu
ytvfsxukgwmlrzdhobec

nrvyx
vytxn
xyvn
nxvy
xnyv

qct
cqt
tcq

y
dy
lcwtunshbmy
fy
ry

cnboedgyxatrk
ktdbeocgnay
wbkgtecnyahmodq
nxbtgzcsyeukrado
ydagcekbfont

jytulh
yhtlz
fwtrkhly
bylht

huigqrok
ikhrong
bighkwor
kgxrhosi

zbkrpelxtsohdaw
ewzkvcxrdtboas
rabkdwoxyneszt
axhkbzrwdteos
rtbowkelsxzad

aonplewqryh
euyqrfncsovkld
byalnwiezroq
enobxrlqy
pmgzeryonql

akxpsmecuj
kfgsexjm
oxqfeskjm
yervixmdktlshbj

r
r
r
r
r

rdghefnubpzkma
dhubafromtpekzgn
knmhfjduebpgraz
zpekdbfamrnihug

ybcgsmlwhovidfezp
omfpzqugycwrhbs

x
x

vjahspyncxzmie
zmpu
krpmzog
tfpmkz

morafxw
xyckafoqwjm
ialpowmf
xhrsmwokbfjazu
anmfowdbq

gdpblscxazjrmikoyth
exrolqfahdbymcwpksjt
gtmadncysrlhpojvxkib

nxdilkwregfbaoqp
exnjrbsifwypkqovag
fgrotinqbwpkxae
flndzeoxkwgrcptibqa

sidjln
mjslodni
ztdoxjnise
odnsehfjti
dnsyij

qx
wmxtj
wxzmtucqy
godkvenhxpb
xrztj

xofhvkjytqus
ytdqjkufimvx
uwyqntcvfpxkj

rdtqukinob
wzufpcolmskynvteh

vnab
kouvrfe
ljvcd
wqbdpvsy
bighvnmx

frgsavwjel
fjlrwsaveg
afeljgrwsv
jlvsrewafg
kvfesjlwgra

ygdszflehmwcnbopiatxju
jdnupgzmilsywaechoft

da
rzfvailh
bhorq
ctgujpmsw

ymbnzldsi
ltvsfm
mgcsl
emslgw

taqecxvdypifblzgk
cekxfvgplrnqbty
vejogtfwqbyknlrpxc

souwzdkcbqf
awzqgvbscukdpxn
bcszjdfkuimwq
sdbcuzqkw

avrlf
kerlgmuahvc
wavfibznrol

upnkam
xmjt
milex

rxotufwczkpvia
pajkvhwtme
gpnektwljasv
pknaqwtvey
wpadktnlvjb

kpfj
syurwad
asyvld

p
bpz
po

s
u
s

ksvbeno
uyfivq
kbjodvh
czptrmawg

xtiqze
xtisdz

pylbdvkncjurmgh
yhbdoltvrpiknmjucg
vbmlhynrcugjpdk

byoavnixtlzepjds
sapxunqeizdvblyho
qvlmeoisanzyhdxpb

ifykcw
iyckw

u
p
s
s

f
f
f
f

fvbjxaiklos
ufqnvcgxikdpalobt
bixlarfkvo

ygpsxce
cpdsfygaxueb
ycgsxep
epcxsgty
pctxesgy

meawzoqnidgh
hodmagqnzew
mwhzndaqgjoe
cdhseoqauwngzmx
oawmeqfdnhzg

yrlvejxocsh
hvjrsmloyne
nsrvyojlhe

zhufxvrkcdmwqlsa
lergvwdqnaosxtzchukbj

xbvyt
ybvndgxt
gzxtonyb
xbcuwtyp

creoxajkgvq
vgljkrexacwmhq

wckjovuftgmb
boktfjuvgmwc

cguaojyvre
ohiknuqags
lcazjyguor

inhyfk
kify

uhtqci
tfoqacim
srvcjytkqgx
hqctz
qmctahizn

vekplqxuhrm
kmeuprlvqxh
vhlrkmpxuqe

at
tagexyi
mpt
yxta

uezmyjqvbgspirlc
fydbljgwmzxquecop
zptrgeybcqmluj
pelzjcugymtbvq

fakguojpbsic
ubcfgyjakpiso
bliusctfvkampjog
fybsicjawkupog

kmxerqlautij
zxidayte

imbngcehtvs
oipfmkrqusnwxdzyl
imasjvn

pek
e
ei
ep
ex

yuw
uyw
wdysu

omikta
zjvatmw
txsma
cm
lndgqmry

cwazbkolvys
liyoveacbswj
siwokbavleyc
rocsyndufablvwx
aylbvcojesw

nazfjsrouhgtv
zgsprothnufav

ptmshxwqk
sebqodwi
euqsw
zwousq

uqltjsikhpvefdawycbzg
plyhvtieubakzswqcfjgd
wehjvikyfpcsgaqtlbuzd
sfibzvquyljekgthpwcda
cgbtvdolizaeqypfksxjuhw

izeqstg
sgiqzet

fqbmpoxscydhrtljzga
cfqodxevmajhbrplzt
oecfrzvbpdaqhtjlxm
adfbxrmqlctzpjho

dsmewbhpvrfuknj
opcvhwtrqkue
rwvkguhpzoe
vxpghreuiwqk

fjzi
ftmizn

xumjz
ujxymg
zumjx
ujmx

n
l
l
x

ghwfslriojz
rfzliwoe
flrziobw
ifcpozlrw

smvfnglxaeczqwukojhbi
uieagsoqbhvlmjcfxwknz
nxoacfwemhijsgqvzbkul
qanmsiwvflcjbxzoeugkh

pzciwneljbghoufdvymtqar
exlhrjvkpnmdoqbtzusiwcgf

uxjwpobgcflndqairke
oktwumrzjlyedagxbinsfp

oibhmyeguqkxjrcpawzfvds
vhsauyxdrobwcjmkzqepgif
gxhiwozfkbsecamrypudvjq
mxywofehvsczapkbuidqjgr
xsmvybwqdgrzeucfahpojik

gdtlkuomn
ytkueoglh

xgimja
gjxibma
mjbagix
xaiojmg
mgjaxi

sgnfdiz
dto

yhkvqrzw
hyknjrzvq
xtrhdsvkzoqy
ykqvzhr
zkqyhanegrv

tzqrhavysuodmjp
abvpofyzhmrjqdt
yzopriuqnvmtahjd
atmryjpzhovqgd

cyljrwofasq
afoqswrjclu
carljswfkoq
ljwsfocqyra
xjolqcwfars

r
o
ak
xdje

lfyjxpoeau
yhlgcftszpr
pkylfomw

lunijqekwg
vafgdsjqeklniw
yeglwqkjin
leqgjkniyw

mpvlijnfbz
jzmfbplnvih
jqdlpvbmnzif

tk
tk
qkst
kt
tk

mdowpcnxgbvuytqezljsk
tbnzcduipewvxlkqrsmjyga

ilumzjpqotcv
fgtmowyx
gdsotkrm

yncslevbgrqmizwu
brclynqgzvuewisfm
lgbzcesuvqnrwmyi

bjmzopcdiqlyueh
olyupebiqcmzjhd

eubsjrvwmyinzkaqdgcf
icqzjksbyregauvfdwnm
jugdnbsczwqfarvkyiem
benyrimfwzaqvukdscjg
ndcuvaziekbwyjrqgsfm

g
c
c

gvyfqupnxtswb
uvtykbjxgifcqz
lhvobuxgqfetmy

dpzgaulb
plqugbrdmac

htr
kgrt
rt
tr
trg

o
qv
lr
m
l

qotxnerijpfbkcagylh
nilhcfrpkjgboxaqey
ognqxipefrbjhlkacy
kojtilqxyrhbpancgfe
qjilgekcfsnhxapomrby

fes
feoijn
ef
ef
ef

dlzga
tlsidg
gdl
lpdgh

onysxwkqlcgdemhup
mzlscwgkdnouexyp
kzdubeoyslgnpcxm
dymulpowksnegxc
kpusvxldgyaonemrci

xh
e

utbirhymae
yzehmoukq

ijvulcmbrgkyxao
uvrajglcbomkxiy

xo
zo
o
xo
o

mbghatcenlifqjdkp
ejscikpgvtomhnxqzf

stpvo
cjvpthsl
spvdnyrt
qvypftsx

obrxaqyudvinmk
qlimnvosubahydgkecrx
yrftzoxkjqvbaidm
kvqaxdmboirwy

dxj
xdj
jxd
jxd
djx

hfjeucmkrvnwxs
nxujekrqmsbofhc

ezarol
lebz
bzel

jgmzcl
jlzcmg
cmlzjg
jmclzg
gmcjzl

o
u

bdfqljiuacptzvgrxoe
pauvztlihdqrjofgx
xtrjvzuaipfldgoq
jzrdtxqlwiavungfpo

cjurgpeyvlzhafx
aurhsfbvpeyxgjzc
gzvhejlkpxyfocura

rvyfbicazduolwtgqk
qklnrgyaofdzvciwut

qsw
qs

nrfgxpujobhk
rxmpnokdgbjfi
lxbngkrfyjvcoz

e
e
zegkx
e
e

kon
k
k

ekvi
iekv
ekvi
ivek

jfbyxwhrne
nxjhrfbdwy
bewhryfjtnx
mfhoxyjbrsnw

bn
n
sxv
d

egrzknhbxfvqtdcu
edhrioxvjgnzbf

yjoskmhw
kdtqfehazbyu

zq
zq
qz
uqz

ilbpcz
ibcqplz
gczbpil

nikychwgab
caibgwklndy
ckbenqwuafyg

qubikjdz
jkiuzdb

gwmavrpfsdnkyizuelt
yuqnvjmlogpizcfetx

tnsf
thnfs
tfsn
sntf
nstf

vflagunhs
aiwvlhjfsunxg
ulvhgfsna

yajtfdwh
hzdpalftjb

nazpqismug
pznuismgq
mgiqnupwsz
mgptzusniq
boifzpmsuqgn

ohu
ht
hpygo
ijkdfhcb
uhno

kfzmrhyjvw
hzgxpjbkwvyr
zykmfrvwshj

yqsezgcojbw
zjsgcoeqby
jgfsbozqyec
czsgoyfbjqe

fjywktd
yjktfwd

nwvrodtpqy
xtrvnosihzgq

gdwcmlxiqtynsjozapeur
xwlgsjatqzycnodprmue
adxtgqujrcopnlwsmezy

snep
shwz

unvpgc
guncvp

suctpbxogra
kcxvgteayu
ztgxuacy

pewbhlvjtg
omskldnz
iafryucxq

zoucjefxpdta
xbfijrelpucsot
ktoexjpcauf
jtezoxkpfhcduw

xiaszncr
asxnircz
xzncrsai
rixseuzanc
nzaicrsx

owtvxqcizrapl
alqxivcprzowt

ltrhdykufvwex
ekxvfldwuhrt
ervktduwlhfjax

ylvgpjthiex
snmbgjfzduxqearikc

eml
bkhyl
rzsoitwqn
dacupjev
fldc

uqigoj
ojui

zgbsumeycaxvontwdkij
jbtelqcxnumsoakiwyvg
waseyvijugnkobptcmx
atesbkmjuhcvfngxywoi

pc
pc
ikyp

xtgvlenosarwubmfihz
txganvweourhbqslfmiz
lientrsuwghobfmvxaz
taluorneizhswmbxgvf
zginuholscexmftrbwva

xjkv
xtouw
tywx
oxwt

zk
cz
bz

qcp
qpc

ogptyeunzsqmarjc
jrvlopzynes
xoszpnjryle
zbrnypsoej

nuigyflbxs
yxunspgj
vnuscztymqowgxr
ixnyshgu

pdn
cyskxdp

ewlkgbdxqtiz
eqgxitldbzkw
kbdexzgitwql

c
p
t
v
gqh

uvigbjqk
ibvugq
vbuiqg

wzdca
dzawc
cawzyd

mfaigwlu
pygxjmufwal
ugfwlam
glmafuw

bewfkxsvl
yxdqnmaturov

fdrbt
mxjnfdqpc
wdltfo
adwletfr

t
pgo
e
tiabc

zbthk
zbkhatr
zbhtk

flxwhbzkei
lwkhxfezbi
xelfwbzhik
bhzefxkliw
klizhwbfxe

btmwkoqs
bstomkqw
wskomtbqi
kswtbmqo
kqbswotm

kdfbzj
bfdzjm

bf
ef
f
fut
bf

bvl
lvu

kezwynmfrgahlbqsjtdxiovpu
wmaukoqsldvheztxrgjpifbny
qgkrwvaiznhpmtlxjubesdoyf
osverdamnxjufcybpkztqighwl

nza
anz

wfjsdaq
iwlrou
ykwftq

zgtuvqxmljwyaifcbp
pcgytlqjvbaxwzimf
cmqzwaljvbufyigptx
sxgvcbweymiflqajtkpz
fhgmjpwclzibtvaqxy

rohqjmnvlsdaigywbxe
xmhiarjdogqwbvnlsey

jhz
jhz
jzh

oemyvhgznpblsq
hpsvzycebqlmng
elhnwqdpbsgzyvm
cenomgyzhqlsbvp

gdnc
wncd

tnlprwbcsaygzqxuoev
rxqdasecgwbzmfvntpl

jtucglizkohrafs
ebjhogu
hdugoj

ncyvqk
oulfaxqnerid
ptmsynbvkwq

mxihwyzetlbnkpfs
ksqwhxtzbenfpim
nqbtxwsepfmkih
khixywsmbnetpfl
peikctmafswhnbx

phylqxrdtg
yxtapqni
nkiwytuveqxp

lhqrfys
sqdfly
ysfhlq
sylfq

sqpjuobilhmtvrxaz
kuwgncfdv

dqzke
qezkd
kqezd
zkqed
kdqez

lio
iol
loi
lio
oil

hvpwi
rhvp
fpysmvd
tpvrl
vph

idjeoxvyqaufgpnlc
xnplcqjvdyigsaef

y
qw

rhb
fpqsiadrjkl

hinvdck
kvcahnid
fhcupntkvdqijmy
dvnchik
aivcnhxkd

xqugmnktofslbpevzd
yaqztjnicrwd

biadvoeznswx
iaxszcdwboj
isbdkxzwoanve
gikqxzorsadfwb
asozrwidqxvb

bvotgwrxe
xbhryeov
ojrvxebi
vxeorb

fb
rqwl
qlrjyfp
nkhgmov

zexjvfbwro
wzvxorfjbe
vorwzexbjtf
xeovwfjbzr

owfmx
uxomf
mfo
cfkmo

atwsbvcn
mijkzngueoy

twqprciyaxkb
barckwiptqyx
iabyqwtcrkxp
rqbkipctxywa

vmhzjek
tx
aix
dsa

byzxqpgureth
gbxryhptqiezuc
lqrzjwythegxubp
hqruytzegbxp
tepughsrqzybx

hwydxbeflai
yhewcfbaixd
yhwadxfeib

hi
h
h
h

mhket
mtkh

loacixzmfy
fmxlzaoyci
ylifaxopczm
czyafoxmli

ymrfxiwbse
ihpqjuaczd

ealixkwz
gocmqyfthp

gedjwnt
wtdezgn
hgcuwrtnped
zjngtdlexw

jk
jk
kj
jk
jk

gkxldasfhqncbiyu
hnpfbycsgqkuldex
hquxfksigydlcnab
cxauqgdiyfbnkshl

niahryvlxbekt
dyhknvbaleirtx
tknljyvrihbzxe
seyxknvihrcbtl
bxrhukldytvneiw

vuq
cfawvlt
rhnvzu

wxlhvnmispbot
whvbcxmolntips
snhtmbpwifulxov
itswhupmncvolbx
rhtlsdpiwonbkqvxm

ojmyc
kfzuehvirps

mk
km
km

fjyx
jitrevpfy
yjs
jyxmiect
dykgjnzbl

rbwpjflnoegkqt
hwxceyimsdzntvu

golf
fzgol
dlxfomg
lfozg
floag

dfbmcyi
yvzpkrbc
icybu

mcbditlrfgeaoh
camdhrfbvoglei
riagefbmclvokqz
dbfogjmeacrpl
lwuogecanfxsbrmy

lgsnwuockhmpxibedqzaf
cvmiepdsubkgwzhf
bsvipdjwcfkmezugh
wgfzicsmubpdkeh

iyeorgvpmutnhqcwxj
jqoguadpnmrk
nmpquzglaorsj

apjqhruongvztmiywecl
padwbieyshonfrtxqcukzglv
yolrhezqicgpmvtaunw
mljzahrwcpqygvonuite

szhjetf
sdtozreuhmfp

f
f
mf
f

gnjqcad
idlhjay
rejbwamoud
danxj
zjkdia

lupiwyqntjgekvsr
gwqupjykitrlnsbo

ojcxmuqsrhyv
lgcovdyhnifsp

wphvcu
lznsm

wpq
qpw
wqp
pwq
fqwhp

nyzx
ubpx
xwi
wtxhe

vkujyxtido
tvouidyjxk
vidkjuoxty
uxtoivdkjy
yvukijdoxt

fpuboyiwdktvc
hsqnzuagr

swabt
mwsb
sbwm
bws

idlshcnf
ficnd
tdfcin
nicdf

ajodwgmsy
wmjvoygd
ojdwmyg
dovgwjmy

yemtzisjpxchanuovwdkr
evxtomkyhrupwsnizac
ytevcsimkwrnzoauhpx

whley
dhyetlqf

fqx
xf
bfxz

ipwvb
vdnb
rcvnf
chvr
xjtvyez

sdpuoewx
wfqrphkdelosbvgyum
pudzioesnw
idatsupewo

knfheau
kanehuf
unekfa
klcsnemaufy

yhuvigcrp
rivgypcuh
urphgvciy
iyhrcvupg
vuphryicg

flozkyvhnwxr
fvpsybhlwrz
hzqvmfgrl
irholckaveszf

gypwufz
agspwqmuyz
yogwpzu

c
oc
`.split('\n\n')