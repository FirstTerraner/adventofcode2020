// agrons way
export function part1Agron(inp: string[]) {
	let sum = 0
	for (const answers of inp) {
		const entries = answers.split('')
		const uniq = [...new Set(entries)].filter(entry => entry !== '\n' && entry !== '\t' && entry !== '\r')
		sum += uniq.length
	}
	return sum
}

// pauls way
export function part1Paul(input: string[][]) {
	const r: string[][] = []
	// tslint:disable-next-line: prefer-for-of
	for (let i = 0; i < input.length; i++) {
		const tmp: string[] = []
		// tslint:disable-next-line: prefer-for-of
		for (let j = 0; j < input[i].length; j++) {
			tmp.push(...input[i][j].length > 1 ? input[i][j].split('') : [input[i][j]])
		}
		r.push([...new Set(tmp)])
	}
	return r.map(x => x.length).reduce((x, y) => x + y)
}
