import { part1Agron, part1Paul } from './1'
import { part2Agron, part2Paul } from './2'
import { agronInp, inputArr, testData } from './input'

describe('test day_6', () => {
	test('test part 1 Agron data', () => { expect(part1Agron(agronInp)).toEqual(6443) })
	test('test part 2 Agron data', () => { expect(part2Agron(agronInp)).toEqual(3232) })

	test('test part 1 testdata', () => { expect(part1Paul(testData)).toEqual(11) })
	test('test part 1 data', () => { expect(part1Paul(inputArr)).toEqual(6735) })
	test('test part 2 testdata', () => { expect(part2Paul(testData)).toEqual(6) })
	test('test part 2 test', () => { expect(part2Paul(inputArr)).toEqual(3221) })
})
