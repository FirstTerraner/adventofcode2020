
// agrons way
export function part2Agron(inp: string[]) {
	let sum = 0
	for (const answers of inp) {
		const entries = answers.split('')
		const uniq = [...new Set(entries)].filter(entry => entry !== '\n' && entry !== '\t' && entry !== '\r')
		sum += uniq.filter(char => answers.split('\n').filter(x => x).every(x => x.includes(char))).length
	}
	return sum
}

// pauls way
export function part2Paul(input: string[][]) {
	let c = 0
	// tslint:disable-next-line: prefer-for-of
	for (let i = 0; i < input.length; i++) {
		const s = [...new Set(input[i])]
		const used:string[] = []
		// tslint:disable-next-line: prefer-for-of
		for (let j = 0; j < s.length; j++) {
			if (s.length === 1) {
				c += s[0].length
				continue
			}
			const allQuest = [...new Set(s.join('').split(''))]
			for (const it of allQuest) {
				if (used.indexOf(it) !== -1){continue}else{used.push(it)}
				if (s.filter(x => x.indexOf(it) !== -1).length === s.length) {c++}
			}
		}
	}
	return c
}
