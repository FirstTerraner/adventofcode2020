// https://jsbench.me/bvkiersyr8/1
// tslint:disable: prefer-for-of

// agrons way
export function part1Agron(inputArr: number[]) {
	for (const it of inputArr) {
		const x = 2020 - it
		if (inputArr.indexOf(x) !== -1) {return it * x}
	}
}

// pauls way
export function part1Paul(inputArr:number[]) {
	for (let x = 0; x < inputArr.length; x++) {
		for (let y = inputArr.length - 1; y >= 0; y--) {
			if (inputArr[x] + inputArr[y] === 2020) { return inputArr[x] * inputArr[y] }
		}
	}
}
