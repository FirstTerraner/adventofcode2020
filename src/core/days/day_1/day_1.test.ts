import { part1Agron, part1Paul } from './1'
import { part2Agron, part2Paul } from './2'
import { inputArr, testData } from './input'

describe('test day_1', () => {
	test('test part 1 test agrons way', () => { expect(part1Agron(inputArr)).toEqual(838624) })
	test('test part 2 test agrons way', () => { expect(part2Agron(inputArr)).toEqual(52764180) })
	test('test part 2 testdata agrons way', () => { expect(part2Agron(testData)).toEqual(241861950) })
	test('test part 1 testdata agrons way', () => { expect(part1Agron(testData)).toEqual(514579) })

	test('test part 1 testdata', () => { expect(part1Paul(testData)).toEqual(514579) })
	test('test part 1 data', () => { expect(part1Paul(inputArr)).toEqual(838624) })
	test('test part 2 testdata', () => { expect(part2Paul(testData)).toEqual(241861950) })
	test('test part 2 test', () => { expect(part2Paul(inputArr)).toEqual(52764180) })
})
