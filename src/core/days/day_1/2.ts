// https://jsbench.me/bvkiersyr8/2

// agrons way
export function part2Agron(inputArr: number[]) {
	// tslint:disable-next-line: prefer-for-of
	for (let i = 0; i < inputArr.length; i++) {
		for (let j = 1; j < inputArr.length; j++) {
			const x = 2020 - inputArr[i] - inputArr[j]
			if (inputArr.indexOf(x) !== -1) {return inputArr[i] * inputArr[j] * x}
		}
	}
}

// pauls way
export function part2Paul(inputArr: number[]) {
	// tslint:disable-next-line: prefer-for-of
	for (let x = 0; x < inputArr.length - 2; x++) {
		for (let y = x + 1; y < inputArr.length - 1; y++) {
			for (let z = y + 1; z < inputArr.length; z++) {
				if (inputArr[x] + inputArr[y] + inputArr[z] === 2020) { return inputArr[x] * inputArr[y] * inputArr[z] }
			}
		}
	}
}
