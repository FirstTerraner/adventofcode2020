import { part1Agron, part1Paul } from './1'
import { part2Agron, part2Paul } from './2'
import { agronsInp, inputData, testData } from './input'

describe('test day_4', () => {
	test('test day4 part1 Agron data', () => { expect(part1Agron(agronsInp)).toEqual(250) })
	test('test day4 part2 Agron data', () => { expect(part2Agron(agronsInp)).toEqual(158) })

	test('test day4 part1 testdata', () => { expect(part1Paul(testData)).toEqual(2) })
	test('test day4 part1 data', () => { expect(part1Paul(inputData)).toEqual(216) })
	test('test part 2 test', () => { expect(part2Paul(inputData)).toEqual(150) })
})
