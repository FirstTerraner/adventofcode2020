interface IValidator { [key: string]: (inp: string) => boolean }

// agrons way
class Passport {
	static validFourDigits(inp: string, min: number, max: number) {
		if (inp.length !== 4) { return false }
		const int = parseInt(inp, 10)
		if (int < min || int > max) { return false }
		return true
	}
	static eyeColors = new Set(['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'])
	static validators: IValidator = {
		byr: inp => Passport.validFourDigits(inp, 1920, 2002),
		iyr: inp => Passport.validFourDigits(inp, 2010, 2020),
		eyr: inp => Passport.validFourDigits(inp, 2020, 2030),
		hgt: inp => {
			if (inp.includes('cm')) { return parseInt(inp, 10) >= 150 && parseInt(inp, 10) <= 193 }
			if (inp.includes('in')) { return parseInt(inp, 10) >= 59 && parseInt(inp, 10) <= 76 }
			return false
		},
		hcl: inp => inp.startsWith('#') && /[0-9a-f]/g.test(inp),
		ecl: inp => Passport.eyeColors.has(inp),
		pid: inp => inp.length === 9 // inp.startsWith('0') not needed :)
	}
	constructor(inp: string) {
		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
		this._map = new Map()
		const list = inp.split(/\s+/g) // split at spaces, tabs, linebreak...
		list.forEach(keyVal => {
			const [key, val] = keyVal.split(':')
			if (key !== '') { this._map.set(key, val) }
		})

	}
	private readonly _entries = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
	private readonly _map: Map<string, string>
	isFully() {
		const pass =  [...this._map.entries()]
		let count = 0
		// eslint-disable-next-line @typescript-eslint/naming-convention
		let valid = false
		for (const [key, val] of pass) {
			if (key !== 'cid') { if (Passport.validators[key](val)) { count++ } }
			valid = count === 7 ? true : false
		}
		return valid
	}
	// isValid() { return this._entries.every(key => this._map.has(key)) }
}
export function part2Agron(inputArr: string[]) {
	let valid = 0
	for (const line of inputArr) {
		const p = new Passport(line)
		if (p.isFully()) { valid++ }
	}
	// console.log('valid passports: ', valid)
	return valid
}

// pauls way
import { parseData } from './1'
// ################################################################## //
// looks like there is no need to check the given rules "to strict" # //
// so i worte this quick´n´dirty way.	 							# //
// ################################################################## //
const fnMap: Map<string, (s: string) => boolean> = new Map([
	// byr (Birth Year) - four digits; at least 1920 and at most 2002.
	['byr', (s: string) => s.length === 4 && +s >= 1920 && +s <= 2020],
	// iyr (Issue Year) - four digits; at least 2010 and at most 2020.
	['iyr', (s: string) => s.length === 4 && +s >= 2010 && +s <= 2020],
	// eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
	['eyr', (s: string) => s.length === 4 && +s >= 2020 && +s <= 2030],
	// hgt(Height) - a number followed by either cm or in:
	// 		If cm, the number must be at least 150 and at most 193.
	// 		If in, the number must be at least 59 and at most 76.
	['hgt', (s: string) =>
		(s.endsWith('cm') && +s.substring(0, s.length - 2) >= 150 && +s.substring(0, s.length - 2) <= 193) ||
		(s.endsWith('in') && +s.substring(0, s.length - 2) >= 59 && +s.substring(0, s.length - 2) <= 76)],
	// hcl(Hair Color) - a # followed by exactly six characters 0 - 9 or a - f.
	['hcl', (s: string) => s.startsWith('#') && s.length === 7],
	// ecl(Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
	['ecl', (s: string) => s === 'amb' || s === 'blu' || s === 'brn' || s === 'gry' || s === 'grn' || s === 'hzl' || s === 'oth'],
	// pid (Passport ID) - a nine-digit number, including leading zeroes.
	['pid', (s: string) => s.length === 9 && isFinite(+s)],
	// cid(Country ID) - ignored, missing or not.
	['cid', (_s: string) => true],
])
function isValidPass(pwData: string[]) {
	const t = pwData.filter(x => fnMap?.get(x.substring(0, 3))?.(x.substring(4))).length
	if (t === 8) { return true }
	let hascid = false
	if (t === 7) {
		pwData.forEach(x => { if (x.startsWith('cid:')) { hascid = true } })
		if (!hascid) { return true }
	}
	return false
}
export function part2Paul(inputData: string) {return parseData(inputData).filter(isValidPass).length}
