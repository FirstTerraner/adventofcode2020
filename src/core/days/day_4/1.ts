
// agrons way
class Passport {
	static entries = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
	constructor(inp: string) {
		this._map = new Map()
		const list = inp.split(/\s+/g)
		// console.log(list)
		list.forEach(keyVal => {
			const [key, val] = keyVal.split(':')
			if (key !== '') { this._map.set(key, val) }
			// console.log(this.map)
		})
	}
	private _map
	isValid() { return Passport.entries.every(key => this._map.has(key)) }
}
export function part1Agron(inputArr: string[]) {
	let valid = 0
	for (const line of inputArr) {
		const p = new Passport(line)
		if (p.isValid()) { valid++ }
	}
	// console.log(valid)
	return valid
}

// pauls way
export function parseData(s: string) {
	return s.split('\n\n')
		.map(x => {
			while (x.indexOf('\n') !== -1 || x.indexOf('  ') !== -1) {
				x = x.replace('\n', ' ').replace('  ', ' ').trim()
			}
			return x.split(' ')
		}).filter(x => x.length > 6)
}
function isValidPass(pwData: string[]) {
	if (pwData.length === 8) { return true }
	if (pwData.length === 7) {
		let hascid = false
		pwData.forEach(x => { if (x.startsWith('cid:')) { hascid = true } })
		if (!hascid) { return true }
	}
}
export function part1Paul(inputData: string) { return parseData(inputData).filter(isValidPass).length }