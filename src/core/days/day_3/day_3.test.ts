import { part1Agron, part1Paul } from './1'
import { part2Agron, part2Paul } from './2'
import { inputArr, testData } from './input'

describe('test day_3', () => {
	test('test part 2 Agron testdata pre', () => {
		expect(part2Agron(testData, 3, true)).toEqual(7)
	})
	test('test part 1 Agron testdata', () => { expect(part1Agron(testData)).toEqual(7) })
	test('test part 1 Agron data', () => { expect(part1Agron(inputArr)).toEqual(272) })
	test('test part 2 Agron testdata', () => {
		expect(
			part2Agron(testData, 1, true) *
			7 *
			part2Agron(testData, 5, true) *
			part2Agron(testData, 7, true) *
			part2Agron(testData, 1, false)
		).toEqual(336)
	})
	test('test part 2 Agron data', () => {
		expect(
			part2Agron(inputArr, 1, true) *
			272 *
			part2Agron(inputArr, 5, true) *
			part2Agron(inputArr, 7, true) *
			part2Agron(inputArr, 1, false)
		).toEqual(3898725600)
	})
	test('test part 2 Agron data pre', () => {
		expect(part2Agron(inputArr, 3, true)).toEqual(272)
	})

	test('test part 1 testdata', () => { expect(part1Paul(testData)).toEqual(7) })
	test('test part 1 data', () => { expect(part1Paul(inputArr)).toEqual(272) })
	test('test part 2 testdata pre', () => {
		expect(part2Paul(testData, 3, 1)).toEqual(7)
	})
	test('test part 2 testdata', () => {
		expect(
			part2Paul(testData, 1, 1) *
			7 *
			part2Paul(testData, 5, 1) *
			part2Paul(testData, 7, 1) *
			part2Paul(testData, 1, 2)
		).toEqual(336)
	})
	test('test part 2 data pre', () => {
		expect(part2Paul(inputArr, 3, 1)).toEqual(272)
	})
	test('test part 2 data', () => {
		expect(
			part2Paul(inputArr, 1, 1) *
			272 *
			part2Paul(inputArr, 5, 1) *
			part2Paul(inputArr, 7, 1) *
			part2Paul(inputArr, 1, 2)
		).toEqual(3898725600)
	})
})
