
// agrons way
export function part2Agron(iArr: string[], xStep: number, odd: boolean) {
	const field: string[][] = []
	const max = iArr.length * xStep
	for (let i = 0; i < iArr.length; i++) {
		const row = iArr[i]
		field[i] = []
		let x = 0
		for (let y = 0; y <= max; y++) {
			if (x === row.length) { x = 0 }
			const c = row[x]
			field[i][y] = c
			x++
		}
	}
	let count = 0
	if (odd) {
		for (let i = 1; i < field.length; i++) { if (field[i][i * xStep] === '#') { count++ } }
	} else {
		for (let i = 0; i < field.length; i += 2) { if (field[i][(i / 2) * xStep] === '#') { count++ } }
	}
	return count
}

// pauls way
export function part2Paul(iArr: string[], xStep:number, yStep:number) {
	const maxX = (iArr.length - 1) * 8
	const arr: string[][] = new Array<string[]>()
	for (let y = 0; y < iArr.length; y ++) {
		arr[y] = []
		let pos = 0
		for (let x = 0; x < maxX; x++) {
			if (pos === iArr[y].length) { pos = 0 }
			arr[y][x] = iArr[y][pos]
			pos++
		}
	}
	let count = 0

	for (let y = yStep; y < arr.length; y += yStep) {
		if (arr[y][(y / yStep) * xStep] === '#') { count++ }
	}

	/* for (let x = 0; x < maxX; x++) {
		process.stdout.write(arr[0][x])
	} */
	return count
}
