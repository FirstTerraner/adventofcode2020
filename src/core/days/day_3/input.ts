export const testData = `..##.........##.........##.........##.........##.........##.......
#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..
.#....#..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.
..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#
.#...##..#..#...##..#..#...##..#..#...##..#..#...##..#..#...##..#.
..#.##.......#.##.......#.##.......#.##.......#.##.......#.##.....
.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#
.#........#.#........#.#........#.#........#.#........#.#........#
#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...
#...##....##...##....##...##....##...##....##...##....##...##....#
.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#`.split('\n').filter(x => !!x)
export const inputArr = `.#...#.......#...#...#.#.#.....
####.....#.#..#...#...........#
.....#...........#......#....#.
......#..#......#.#..#...##.#.#
............#......#...........
...........#.#.#....#.......##.
....#.......#..............#...
........##...#.#.....##...##.#.
.#.#.....##................##..
.##................##..#...##..
....#...###...##.........#....#
.##......#.........#...........
...#.#.#....#....#...#...##...#
..#....##...#..#.#..#.....#.#..
.......#...#..#..#.....#...#..#
.....#......#.......#.....#.#..
....#..#...#..#####....##......
.#...........#......#....#....#
#......#.###.....#....#....#...
....#..#.#.#..#...........##...
..#..#..#.#...#......#....#.##.
.##....#......#...#.#..#.......
..###.#...#.........#.#.#...#.#
#....###.........#...#...#...#.
...##.#............#...##......
...#.........#............#....
......##...#...##..#...........
........##..#.#.####...#.....#.
.##.........#......#..#..#...#.
..........#...#..........#.....
#..........#........#..#..#.#..
..#....#.#.#.#.#..#.##.........
##.#.#.##.....#..#......###....
##....#...#.....#..............
.#..#...#...#....###......#....
#....#......#.#..#.#........###
.#....#..#...###....#...##.....
.#....#.....#.....#..##..#.....
#....#.##...#...#..#.##.##.#...
.#.#.#.##...#####.............#
......##..#.....##..#...####...
#.##..#.#....#..##.......###..#
..#.......##....#........#.##..
#.....#......#.....#....#..#...
.......##...#.....##.......#..#
.......#...#.#.#.........#####.
#.......#.##..##........##.....
##..#...#........##....#.......
.......#...##......##...##.##..
......#..##..#.#...#...#....##.
....#.#..#.....#.##.#.....#.#..
#..#.#.#........#...#.......##.
##...........#..#........#.....
....##....#....#.#.......#.....
....##.#.#.....#.#.....#.....#.
..........#.#..##..#..#.......#
#....#.......##...#...#.....#..
.........##.....#.#....#......#
..........#........#..#..#.#...
..#......#.....#......#......#.
..#...###..##..#.....##..#..##.
..#.#..###.........#.#...##.#.#
#.........#..#......#...##.....
...#...#.#..#...##.#...##.#..#.
#.....#.....#.##....#.#......#.
#....##..##..#.#..##....#.....#
.#..........#..#...#..#.......#
#.#.....#..##..##..#.#.........
....#..##...#.....#.....#.#.#.#
...#.#....#........#...#.#.....
.#............#.......#.##.#...
..##.......#.#...#........##..#
..................##.#...#.#..#
.#.........#.......#.....#.....
....##...##..#..........#......
..#.##..#....#..#............#.
....####...#.##....##.#....#.##
#..#....#......##........##....
..###...........##..#......#...
#..#.......#........#.......#..
.....#....#..#..##.....#.......
.###.####.#....#....#..#.......
.............#...............#.
.#..........#.#....#..#.#......
..............##....#..#....##.
.......#.#..#........#.......##
#..#...#..#.#........#..#....#.
...#.........#...#..#..........
...#....##...#..#..........#...
.#......#......##..##...#.#....
.#.........#..###..............
.................#.#.....##....
...#............#..............
#..#................#.......#..
...#.......#......#.#.#........
#.....#.##....#.....#........#.
......##.#....#........#....#..
.#..#.##...##........#.#.....#.
..#...#....#...#..#..##..#.#..#
#.................#.#.......##.
..........#........#.#.....#..#
#....##....#........##..##.#...
#...#....#.....#.....#.....#...
#..#..........#....##....#....#
..#.#..#..#....#.#....#....#..#
#....#..#.......#..........#...
.#...#.#...#..#...#.......#....
###........#......##..#...##...
...#..........##..............#
.......#........##......#.....#
.#..........#...#...##....###.#
.#...#....#..#.....##...#..##..
.#.#.#...##..........##...#...#
.#.....#...#........#........##
#.......#......##.#.#..#....#..
##..#.##........#....#..#......
...#.......................#...
..#....#..##........##.#.##.#..
.............#.......#....#.#.#
...#...........##..#.....#.....
..#....##....#.....#...........
..#.....#......#..#.###.##....#
.#.......#...........#...#....#
#............##...#...#.....#..
##...#.....#.........##...##...
...#...........#....##.........
#.##..#..........##..#......#..
.......#.#.......##.......#....
..#.....##..#...#.......#......
.#........#....##...........#..
#.......#...#.#.###...#....#...
..........##..#..#..##........#
#....#....#...#....#....#......
...........#....#...#...##.#...
.........#.#.....#.............
..####...........##..........#.
.....#...................#.....
#..##...#........#.###.#.##....
....##...#.##................#.
.#........###.#............#.#.
..............#.##.........#...
##............#.#..###....#...#
#.....#........####....#....##.
....##..#...##..##...##.....#..
##..#....#.##.....####.....#.##
##..#....#.##.##.#.#........#..
....#..........##.....#..#..#..
...#.......#........#.........#
#..##.######.......##........#.
###...#...####.......#.....#...
#......#..#.....#......#.....#.
..................##...#.......
....#.#....#......#...#.....##.
..#..#..#..#..#....#.#...#....#
......#....###.................
#.##......#...#......#.........
#..#.#...##..#.......#..#...#..
.#....#.#........#.........#...
#.......##..#..#...............
........#..##....#.....#..#....
....#......##..#....#...#..#...
#.....#...##..#...#......#.....
.....#.....#.........##...#..#.
........#...##.#...#.#....#..##
....#....#...#.....##..#...#...
#....#..#.........#.........###
..###.....##...#.#....##......#
#..#.#..#.......#..#....##.....
###...#.##..#.......#......#...
.....#.....##.......#...##..#..
......#.......#.#.#......#..#..
.................##..#.###.....
..........#....#...#..........#
...#.#...#.#..##.....#.#.##..#.
.......#..#....#...#......###..
...##..........#..#.....#....#.
.#..##..###...#....##.....#....
..#.#..............#....#...#..
.....####.......#.#.##....#....
#.#.#..##.##.#..#.##.#....#..#.
........#....#.......##........
...#...#....#...###.....###....
.....#..#..........##.#...##.##
..#.#.#..#....#...#..##...#...#
..#......#..#.#.....#....#....#
.#.....#.......#............#..
#..##....#...#....##....#......
#..#.........#...#...###.#..#..
..#.#.#..#.#..#.......##.......
...##...............#..#...#.#.
.......####.#.....#..#..#......
......#..#.....#..##....#......
....#...#.........##.......#.#.
#.#.#...#.....#...#..#.#..#....
........#..#.........#..#..##..
........###.#............#.#...
#..#.......#.#..#.......#...#.#
..##..............#.#.....#...#
..##...........................
..#.....#.......#......##......
#...#......##.#....#.#.#...##.#
#...#.#......#.#..##.........#.
.##..#...#.#.....#.#.#...#.#..#
.#..#...#.#.........#......#...
...........#...#...#...#..#.#..
.#........#...#......##...#.###
#........#..#.#..#...........##
.#...#...####.......#..........
......#...............#........
.....#.#.....#.#...#......#....
.#........#...........#..##.#..
....#..#.....###.......#...#...
#.#.........#...##..#.#.##.#...
................##.#....#.#...#
.......#.......#......#...#....
#....#.#..............#.##..###
..##.##..#.....#............#..
#....#..##........#....#.......
.#.#........#.#................
......##..#..#..........#..#.#.
.....##.#..#....##.#......##...
........###.#................#.
#..###.....#.###.#...#.#.......
.#..#.#.#.#..#..#.#.....#.#....
#....#.....#..#......##...#..##
........#...##..#.#.....#....#.
.......#..#..#..#....#.....##..
....#..##..#...#....#.........#
#.#....#..#.#...#.#...#....#...
.....#......###.......#..##.#.#
.......##.....#....#........#.#
.##.##..#..###.#....#.#.....#..
..##.#.......###.........#.....
.#...#......#..#....#..........
.....#........#.....##...#.....
..#......#.#.#..#.#....##.#...#
#.#...#...........##......#....
.................##.....#.#.##.
###..#....#..................#.
##..#.#.#...#....###.#.#...##.#
#.#.#..#....#..............#...
.....#....#......#..#.##.......
#...#...#..###.......#.......#.
.....#.#........#..#...#.#.....
.....#..........#.###.......#..
...#.##.....#....###.....#.....
####........#....#..#.#.##.#...
#......#...##.....#.#..##.#.#.#
.....##....#..#.........##.....
..##....##................##..#
#.....#...##...##.#.....#...#..
..#..#.#.#....#.#.......#......
##.....##......#...#.........#.
#..........#........#.#......#.
.#..#.......#.#.....#..........
.........#.#.......#.#..#..#..#
#......#....#....#..##..##...##
.....#..#...#.......#.....##...
..#.##........#.###...#...#...#
..#..#...........#..........#..
.#.#.#...#.##.#..............#.
....#..##.......#.....#..##..#.
.#.##.#....##........#...##.##.
...#.#...#....#....#......#####
.....#.....##...........#......
#........#.##.......#.#.......#
#...#.......##.#.......#..#.#..
#...##..#....#............#.#..
........#.#..#...#..#...##..##.
#...#....#............#........
#.#.#.#.#....##.....##.........
......##.........#.......#.#..#
...#.#....#........#...........
...#.#.......#.....#...........
##....####......##.##..#.......
#......#...#..#.#..#......#..#.
#......#.#....#....#..#........
..#.###...#.....#........#.#...
..#.....##.....###....#.....#..
#.##.#.....##....#...###.......
###.#....###.#..##.#.......##.#
#..#..##...#.#..........##.##..
.......####.#..#.....##..###...
#...#...##..#..##.......###....
#....#.........##..#.........#.
.....#.#..........#..#...#.#..#
..........#......##..#..#.#....
.#...#...#...#........###....##
#....#.##..........#.#.....#.#.
#....##.#.##..#.......#.#.....#
.##..##..#.#...#.#...........#.
....##..#...#.#.##.#.#...#.....
.#...#.##........#.##..#.#....#
.#.....##.........#.....#......
..#.....#.#..#.##.............#
##....##...#....##........#....
.#....#........#.#..#..#..#.##.
.#........#............#.......
.#..##..##..#..#..####....#....
..#.###....#..#.##......#.#...#
.###..#.#...##....##....#..##.#
....##........#....#.#.#...##..
...#..#....#.#....#...#.#.....#
...##....##..#....#.........#..
.....#..##.###..#.....####.....
...#..#.........#....#.#.##..#.
...#..#...............#..#....#
...........#.....#...####..##.#
..#......#...#....#..#...##.#..
.....#..#...........#.......#.#
##....###...#.........#....#...
...#..##.......#.#.....##....#.
#.#...#.#....#.....#...##.....#
.#...##....#.....#..##.......#.
...#........##..........#.....#
#...##..#.#....###...#..#......
............#.......#......#.#.
......#....#.#...#...#..#......
.#..#......#....#.......#....##
...#...#.......###..###...#....
.............#.#...#..###.....#
.#.....#........#...##....#..#.
.....#.......#######.#.#...#...
`.split('\n').filter(x => !!x)
