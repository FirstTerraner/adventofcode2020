/* eslint-disable @typescript-eslint/no-unsafe-assignment */

// agrons way
export function part1Agron(iArr: string[]) {
	const field: string[][] = []
	const max = iArr.length * 3
	for (let i = 0; i < iArr.length; i++) {
		const row = iArr[i]
		field[i] = []
		let x = 0
		for (let y = 0; y <= max; y++) {
			if (x === row.length) { x = 0 }
			const c = row[x]
			field[i][y] = c
			x++
		}
	}
	let count = 0
	for (let i = 1; i < field.length; i++) { if (field[i][i * 3] === '#') { count++ } }
	return count
}

// pauls way
export function part1Paul(iArr: string[]) {
	const maxX = (iArr.length - 1) * 4
	// console.log(maxX, maxY)
	const arr: string[][] = new Array<string[]>()
	for (let y = 0; y < iArr.length; y++) {
		arr[y] = []
		let pos = 0
		for (let x = 0; x < maxX; x++) {
			if (pos === iArr[y].length) { pos = 0 }
			arr[y][x] = iArr[y][pos]
			pos++
		}
	}
	let count = 0

	for (let y = 0; y < arr.length; y++) {
		if (arr[y][y * 3] === '#') { count++ }
	}

	/* for (let x = 0; x < maxX; x++) {
		process.stdout.write(arr[0][x])
	} */
	return count
}
// part1(inputArr)
