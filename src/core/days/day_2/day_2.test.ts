
import { part1Agron, part1Paul } from './1'
import { part2Agron, part2Paul } from './2'
import { inputArr, testData } from './input'

describe('test day_2', () => {
	test('test part 1 testdata agron', () => { expect(part1Agron(testData)).toEqual(2) })
	test('test part 1 test agron', () => { expect(part1Agron(inputArr)).toEqual(607) })
	test('test part 2 testdata agron', () => { expect(part2Agron(testData)).toEqual(1) })
	test('test part 2 test agron', () => { expect(part2Agron(inputArr)).toEqual(321) })

	test('test part 1 testdata', () => { expect(part1Paul(testData)).toEqual(2) })
	test('test part 1 data', () => { expect(part1Paul(inputArr)).toEqual(607) })
	test('test part 2 testdata', () => { expect(part2Paul(testData)).toEqual(1) })
	test('test part 2 test', () => { expect(part2Paul(inputArr)).toEqual(321) })

})
