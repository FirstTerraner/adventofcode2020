
// agrons way
// https://jsbench.me/b9kig26i0a/2
export function part2Agron(arr: string[]) {
	let valid = 0
	// tslint:disable-next-line: prefer-for-of
	for (let i = 0; i < arr.length; i++) {
		const partsArr = arr[i].split(' ')
		const int = partsArr[0].split('-')
		const i1 = +int[0]
		const i2 = +int[1]
		const needle = partsArr[1]
		const hackystack = partsArr[2]
		if ((hackystack[i1 - 1] === needle[0] && hackystack[i2 - 1] !== needle[0]) ||
			hackystack[i1 - 1] !== needle[0] && hackystack[i2 - 1] === needle[0]) {
			valid++
		}
	}
	return valid
}

// pauls way
export function part2Paul(arr: string[]) {
	// console.log('start', arr)
	return arr.map(x => {
		const tmpArr = x.split(' ')
		const tmpArr2 = tmpArr[0].split('-')
		return {
			char: tmpArr[1].substring(0, 1),
			pos: tmpArr2[0],
			pos2: tmpArr2[1],
			pw: tmpArr[2]
		}
	}).filter(x => {
		{
			/* 	console.log(x,
					x.pw.substring(+x.pos - 1, +x.pos), x.pw.substring(+x.pos - 1, +x.pos) !== x.char,
					x.pw.substring(+x.pos2 - 1, +x.pos2), x.pw.substring(+x.pos2 - 1, +x.pos2) !== x.char) */
			return !((x.pw.substring(+x.pos - 1, +x.pos) === x.char) === (x.pw.substring(+x.pos2 - 1, +x.pos2) === x.char))
		}
	}
	).length
}
