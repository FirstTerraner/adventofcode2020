
// agrons way
// https://jsbench.me/b9kig26i0a/1
export function part1Agron(arr: string[]) {
	let valid = 0
	// tslint:disable-next-line: prefer-for-of
	for (let i = 0; i < arr.length; i++) {
		const partsArr = arr[i].split(' ')
		const int = partsArr[0].split('-')
		const min = +int[0]
		const max = +int[1]
		const needle = partsArr[1]
		const hackystack = partsArr[2]
		let count = 0
		// tslint:disable-next-line: prefer-for-of
		for (let y = 0; y < hackystack.length; y++) { if (hackystack[y] === needle[0]) { count++ } }
		if (count >= min && count <= max) { valid++ }
	}
	return valid
}

// pauls way
// ['1-3 a: abcde', '1-3 b: cdefg', '2-9 c: ccccccccc']
const countSubstrInStr = (str: string, substr: string) => {
	let count = 0
	// tslint:disable-next-line: prefer-for-of
	for (let i = 0; i < str.length; i++){
		if (str.substring(i, i + 1) === substr) {count++ }
	}
	return count
}
export function part1Paul(arr: string[]) {
	// console.log('start', arr)
	return arr.map(x => {
		const tmpArr = x.split(' ')
		const tmpArr2 = tmpArr[0].split('-')
		return {
			char: tmpArr[1].substring(0, 1),
			min: tmpArr2[0],
			max: tmpArr2[1],
			pw: tmpArr[2]
		}
	}).filter(x => {
		// console.log(x)
		const count = countSubstrInStr(x.pw, x.char)
		return count >= +x.min && count <= +x.max
	}).length
}
