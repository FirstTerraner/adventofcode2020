
// agrons way
export function part1Agron(inputArr: string[]) {
	const finalValues = []
	for (const sitId of inputArr) {

		const chars = sitId.split('')
		let minRow = 0
		let maxRow = 127
		let minCol = 0
		let maxCol = 7
		let finalRow = 0
		let finalCol = 0

		chars.forEach(char => {
			const rowsLeft = maxRow - minRow
			const colLeft = maxCol - minCol
			const halfRow = rowsLeft / 2
			const halfCol = colLeft / 2
			// console.log('rowsLeft: ', rowsLeft)
			// console.log('colLeft: ', colLeft)
			if (char === 'F'){
				if (rowsLeft === 1) { finalRow = minRow }
				const nextMax = maxRow - halfRow
				// console.log(Math.floor(maxCol - halfCol))
				maxRow = Math.floor(nextMax)
			}
			if (char === 'B'){
				if (rowsLeft === 1) { finalRow = maxRow }
				minRow = Math.ceil(maxRow - halfRow)
			}
			if (char === 'L'){
				if (colLeft === 1) { finalCol = minCol }
				maxCol = Math.floor(maxCol - halfCol)
			}
			if (char === 'R'){
				if (colLeft === 1) { finalCol = maxCol }
				minCol = Math.ceil(maxCol - halfCol)
			}
		})
		finalValues.push([finalRow, finalCol])
	}

	const idArr = []
	for (const vals of finalValues) { idArr.push((vals[0] * 8) + vals[1]) }
	return idArr.sort((a, b) => a - b).pop()
}

// pauls way
/*
Instead of zones or groups, this airline uses binary space
partitioning to seat people. A seat might be specified
like FBFBBFFRLR,
where F means "front", B means "back", L means "left", and R means "right".

The first 7 characters will either be F or B; these specify exactly
one of the 128 rows on the plane (numbered 0 through 127).
Each letter tells you which half of a region the given seat is in.
Start with the whole list of rows; the first letter indicates whether
the seat is in the front (0 through 63) or the back (64 through 127).
The next letter indicates which half of that region the seat is in,
and so on until you're left with exactly one row.

For example, consider just the first seven characters of FBFBBFFRLR:
	Start by considering the whole range, rows 0 through 127.
	F means to take the lower half, keeping rows 0 through 63.
	B means to take the upper half, keeping rows 32 through 63.
	F means to take the lower half, keeping rows 32 through 47.
	B means to take the upper half, keeping rows 40 through 47.
	B keeps rows 44 through 47.
	F keeps rows 44 through 45.
	The final F keeps the lower of the two, row 44.
*/

// ######################################### //
// im pretty sure this is not 100% correct # //
// but it works and it is late. 		   # //
// ######################################### //
export function decodeRow(s: string){
	s = s.substr(0, 7)
	let rows: number[] = [...Array(128).keys()]
	// tslint:disable-next-line: prefer-for-of
	for (let i = 0; i < s.length; i++) {
		rows = s[i] === 'F'
			?
			rows.filter(x => x - rows[0] < rows.length / 2)
			:
			rows.filter(x => x >= (rows.length / 2) + rows[0])
	}
	return rows[0]
}
export function decodeColumn(s: string) {
	s = s.substr(7)
	let columns: number[] = [...Array(8).keys()]
	// tslint:disable-next-line: prefer-for-of
	for (let i = 0; i < s.length; i++) {
		// console.log('pre', s[i],columns)
		columns = s[i] === 'L'
			?
			columns.filter(x => x - columns[0] <= (columns.length / 2) - 1)
			:
			columns.filter(x => x >= (columns.length / 2) + columns[0])
	}
	return columns[0]
}
export function decode(s: string) { return decodeRow(s) * 8 + decodeColumn(s)}
export function part1Paul(inputArr: string[]) { return Math.max(...inputArr.map(x => decode(x))) }
