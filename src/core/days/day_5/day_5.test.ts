import { decodeColumn, decodeRow, part1Agron, part1Paul } from './1'
import { part2Agron, part2Paul } from './2'
import { agronInput, inputArr, testData } from './input'


describe('test day_5', () => {
	test('test agron day 5 part 1', () => { expect(part1Agron(agronInput)).toEqual(944) })
	test('test agron day 5 part 2', () => { expect(part2Agron(agronInput)).toEqual(554) })

	test('test part 1 decodeRow test', () => { expect(decodeRow(testData[0])).toEqual(44) })
	test('test part 1 decodeRow test2', () => { expect(decodeRow(testData[1])).toEqual(70) })
	test('test part 1 decodeRow test3', () => { expect(decodeRow(testData[2])).toEqual(14) })
	test('test part 1 decodeRow test4', () => { expect(decodeRow(testData[3])).toEqual(102) })
	test('test part 1 decodeColumn test', () => { expect(decodeColumn(testData[0])).toEqual(5) })
	test('test part 1 decodeColumn test2', () => { expect(decodeColumn(testData[1])).toEqual(7) })
	test('test part 1 decodeColumn test3', () => { expect(decodeColumn(testData[2])).toEqual(7) })
	test('test part 1 decodeColumn test4', () => { expect(decodeColumn(testData[3])).toEqual(4) })
	test('test part 1 testdata', () => { expect(part1Paul(testData)).toEqual(820) })
	test('test part 2 test data', () => { expect(part2Paul(inputArr)).toEqual(587) })
})
