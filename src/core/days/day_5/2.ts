
// agrons way
export function part2Agron(inputArr: string[]) {
	const finalValues = []
	for (const sitId of inputArr) {

		const chars = sitId.split('')
		let minRow = 0
		let maxRow = 127
		let minCol = 0
		let maxCol = 7
		let finalRow = 0
		let finalCol = 0

		chars.forEach(char => {
			const rowsLeft = maxRow - minRow
			const colLeft = maxCol - minCol
			const halfRow = rowsLeft / 2
			const halfCol = colLeft / 2
			// console.log('rowsLeft: ', rowsLeft)
			// console.log('colLeft: ', colLeft)
			if (char === 'F'){
				if (rowsLeft === 1) { finalRow = minRow }
				const nextMax = maxRow - halfRow
				// console.log(Math.floor(maxCol - halfCol))
				maxRow = Math.floor(nextMax)
			}
			if (char === 'B'){
				if (rowsLeft === 1) { finalRow = maxRow }
				minRow = Math.ceil(maxRow - halfRow)
			}
			if (char === 'L'){
				if (colLeft === 1) { finalCol = minCol }
				maxCol = Math.floor(maxCol - halfCol)
			}
			if (char === 'R'){
				if (colLeft === 1) { finalCol = maxCol }
				minCol = Math.ceil(maxCol - halfCol)
			}
		})
		finalValues.push([finalRow, finalCol])
	}

	const idArr = []
	for (const vals of finalValues) { idArr.push((vals[0] * 8) + vals[1]) }
	const sortedIds = idArr.sort((a, b) => a - b)
	for (let i = 0; i < sortedIds.length; i++) {
		const current = sortedIds[i]
		if (sortedIds[i + 1] === current + 2) { return current + 1 }
	}
}

// pauls way
import { decode } from './1'
export function part2Paul(inputArr: string[]) {
	const ids = inputArr.map(x => decode(x))
	// airplain seat rows * seat columns
	const seatCount = 128 * 8
	/*Your seat wasn't at the very front or back,
	 though; the seats with IDs +1 and -1 from yours will be in your list. */
	const skipCount = 100
	const all = [...Array(seatCount).keys()]
		.filter(x => x > skipCount && !ids.includes(x) && x < (seatCount) - skipCount)
	// console.log(all)
	return all[0]
}
