# adventOfCode
[![pipeline status][PipelineStatusBadge]][ProjectUrl]
[![coverage report][CoverageBadge]][ProjectUrl]

## targeting **ESnext** - tested with node **>=** v**15.3.0**
<!-- because i ❤ [Optional chaining](https://2ality.com/2019/07/optional-chaining.html) -->

## Installing adventOfCode from source

### Linux, macOS, Windows, *BSD, Solaris, WSL, Android, Raspbian

```bash
git clone https://gitlab.com/FirstTerraner/adventofcode2020.git
cd adventOfCode2020
npm i
# new day # 1 = first day
scripts/nday.sh 1
npm run test
```

That's it!


next day 

```bash
# new day # 2 = second day
scripts/nday.sh 2
npm run test
```














[PipelineStatusBadge]: https://gitlab.com/billigsterUser/adventOfCode/badges/master/pipeline.svg
[CoverageBadge]: https://gitlab.com/billigsterUser/adventOfCode/badges/master/coverage.svg
[ProjectUrl]: https://gitlab.com/billigsterUser/adventOfCode/commits/master
