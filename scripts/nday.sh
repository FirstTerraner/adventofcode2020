echo "Script executed from: ${PWD}"
cd $(dirname $0) && cd ..

echo $PWD
###opts
set -e #exit on error
#set -x #debug
set -o pipefail #fail on error in pipechain
if [ "$1" == "" ]; then
	echo "pls add a day (number) like: ./${0} 2"
else
	mkdir -p ./src/core/days/day_${1}
	cd ./src/core/days/day_${1}
	echo 'export function part1(inputArr:number[]) {return null}' >>1.ts
	echo 'export function part2(inputArr:number[]) {return null}' >>2.ts
	echo 'export const testData = [];export const inputArr = []' >>input.ts
	echo "import { part1} from './1'
import { inputArr, testData } from './input'

describe('test day_1', () => {
	test('test part 1 testdata', () => { expect(part1(testData)).toEqual(514579) })
	test('test part 1 data', () => { expect(part1(inputArr)).toEqual(838624) })

	/* test('test part 2 testdata', () => { expect(part2(testData)).toEqual(241861950) })
	test('test part 2 test', () => { expect(part2(inputArr)).toEqual(52764180) }) */
})" >>day_1.test.ts
	npm run b
fi
